package se.kstudios.colorblobs.controllers.input;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Vector2;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;

import se.kstudios.colorblobs.controllers.GameplayCamera;
import se.kstudios.colorblobs.models.Blob;
import se.kstudios.colorblobs.views.RenderOptions;

/**
 * Input processor for the gameplay. Updates model based on input events from mouse, keyboard and touch screen.
 *
 * @author kristofer
 */
public abstract class GameplayInputProcessor implements InputProcessor {

    protected HashMap<Integer, Vector2> activePointerPositions;
    protected HashMap<Integer, Vector2> activePointerStartPositions;
    protected Blob targetBlob;
    protected GameplayCamera camera;
    protected int screenHeight;

    private Vector2 twoPointerDrag;
    private Vector2 lastTwoPointerDrag;
    private float accumulatedAngleDelta;
    private float dragLengthToDoubleZoom;

    public GameplayInputProcessor(Blob targetBlob, GameplayCamera camera, int screenWidth, int screenHeight) {
        activePointerPositions = new HashMap<>();
        activePointerStartPositions = new HashMap<>();
        this.targetBlob = targetBlob;
        this.camera = camera;
        this.screenHeight = screenHeight;
        lastTwoPointerDrag = new Vector2(0, 0);
        twoPointerDrag = new Vector2(0, 0);
        accumulatedAngleDelta = 0f;
        dragLengthToDoubleZoom = Math.min(screenWidth, screenHeight) / 1.5f;
    }

    @Override
    public boolean keyDown(int keycode) {
        switch (keycode) {
            case Input.Keys.D:
                RenderOptions.debugRender = !RenderOptions.debugRender;
                return true;
            case Input.Keys.P:
                RenderOptions.renderPlayerBlobHud = !RenderOptions.renderPlayerBlobHud;
                return true;
            case Input.Keys.B:
                RenderOptions.renderBlobMemberTexts = !RenderOptions.renderBlobMemberTexts;
                return true;
            case Input.Keys.S:
                RenderOptions.renderScreenHud = !RenderOptions.renderScreenHud;
                return true;
            case Input.Keys.A:
                RenderOptions.renderCameraHud = !RenderOptions.renderCameraHud;
                return true;
            case Input.Keys.M:
                RenderOptions.renderMapHud = !RenderOptions.renderMapHud;
                return true;
            case Input.Keys.C:
                if (camera.cameraMode == (GameplayCamera.CameraMode.TIGHT)) {
                    camera.cameraMode = GameplayCamera.CameraMode.LOOSE;
                } else {
                    camera.cameraMode = GameplayCamera.CameraMode.TIGHT;
                }
                return true;
            case Input.Keys.R:
                camera.rotate(-10);
                return true;
            case Input.Keys.T:
                camera.rotate(10);
                return true;
            case Input.Keys.PLUS:
                camera.zoom += 5;
                return true;
            case Input.Keys.MINUS:
                camera.zoom -= 5;
                return true;
            case Input.Keys.SLASH:
                targetBlob.setMass(targetBlob.getMass() * 0.75f, true);
                return true;
            case Input.Keys.STAR:
                targetBlob.setMass(targetBlob.getMass() * 1.25f, true);
                return true;
            default:
                return false;
        }
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        registerPointer(pointer, screenX, screenY);
        resetAccumulatedAngleDelta();
        if (pointerCount() == 2) {
            updateDragVector(lastTwoPointerDrag);
        }
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        unregisterPointer(pointer);
        resetAccumulatedAngleDelta();
        if (pointerCount() == 1) {
            unregisterPointer(0);
        } else if (pointerCount() == 2) {
            updateDragVector(lastTwoPointerDrag);
        }
        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        updateActivePointerPosition(pointer, screenX, screenY);
        if (pointerCount() == 2) {
            updateDragVector(twoPointerDrag);
            updateZoom();
            updateRotation();
            updateDragVector(lastTwoPointerDrag);
        }
        return true;
    }

    @Override
    public boolean scrolled(int amount) {
        camera.zoom += amount;
        return true;
    }

    public abstract void update(float delta);

    protected int pointerCount() {
        return activePointerPositions.size();
    }

    protected Vector2 pointerStartPosition(int orderIndex) {
        return pointerPosition(activePointerStartPositions, orderIndex);
    }

    protected void resetStartPosition(int orderIndex) {
        pointerStartPosition(orderIndex).set(pointerPosition(orderIndex));
    }

    protected Vector2 pointerPosition(int orderIndex) {
        return pointerPosition(activePointerPositions, orderIndex);
    }

    private void updateDragVector(Vector2 dragVector) {
        assert (pointerCount() == 2);
        Vector2 pointer0 = pointerPosition(0);
        Vector2 pointer1 = pointerPosition(1);
        dragVector.x = pointer0.x - pointer1.x;
        dragVector.y = pointer0.y - pointer1.y;
    }

    private void resetAccumulatedAngleDelta() {
        accumulatedAngleDelta = 0f;
    }

    private void updateZoom() {
        float length = twoPointerDrag.len();
        float lastLength = lastTwoPointerDrag.len();
        float lengthDelta = lastLength - length;
        float zoomModifier = lengthDelta / dragLengthToDoubleZoom;
        camera.zoom = camera.zoom + zoomModifier * camera.zoom;
    }

    private void updateRotation() {
        float angleDelta = lastTwoPointerDrag.angle() - twoPointerDrag.angle();
        if (Math.abs(accumulatedAngleDelta) > 10) {
            camera.rotate(angleDelta);
        } else {
            accumulatedAngleDelta += angleDelta;
        }
    }

    private void updateActivePointerPosition(int pointer, int x, int y) {
        if (activePointerPositions.containsKey(pointer)) {
            Vector2 position = activePointerPositions.get(pointer);
            position.set(x, y);
        }
    }

    private void registerPointer(int pointer, int positionX, int positionY) {
        activePointerPositions.put(pointer, new Vector2(positionX, positionY));
        activePointerStartPositions.put(pointer, new Vector2(positionX, positionY));
    }

    private void unregisterPointer(int pointer) {
        activePointerPositions.remove(pointer);
        activePointerStartPositions.remove(pointer);
    }

    private Vector2 pointerPosition(HashMap<Integer, Vector2> pointerMap, int orderIndex) {
        LinkedList<Integer> sortedPointers = new LinkedList<Integer>(pointerMap.keySet());
        Collections.sort(sortedPointers);
        int pointer = sortedPointers.get(orderIndex);
        return pointerMap.get(pointer);
    }
}