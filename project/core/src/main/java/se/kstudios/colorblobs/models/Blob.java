package se.kstudios.colorblobs.models;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;

import se.kstudios.colorblobs.models.ai.BlobBehavior;
import se.kstudios.colorblobs.utils.MathUtil;

public class Blob extends Box2dWorldUser implements Disposable {

    public final static float APPLY_FORCE_OVER_TIME_TIME = 0.25f; //sec
    private final static float APPLY_FORCE_OVER_TIME_FORCE_PER_MASS = 1000f; //newton per kg
    private final static float APPLY_FORCE_OVER_TIME_IMPROVED_MODIFIER = 5f;
    private final static float MAX_APPLY_FORCE = 14000f; //newton
    private final static float VIEW_DISTANCE_MODIFIER = 10f;
    private final static float MAX_VIEW_DISTANCE = 500f;
    private final static float OTHER_BLOB_MERGE_COLOR_MODIFIER = 0.75f;

    private String name;
    private Color color;
    private BlobBehavior behavior;
    private Body body;
    private Fixture fixture;

    private Vector2 acceleration;

    private Vector2 appliedForce;
    private Vector2 appliedForceOverTime;
    private float timeAppliedForceOverTime;
    private float timeToApplyForceOverTime;

    private Vector2 dragForce;
    private float dragCoefficient;
    private Vector2 totalAppliedForce;
    private Vector2 totalForce;

    public Blob(float radius, float density, float friction, float restitution, Color color) {
        name = getWorldManager().getNextObjectName(this.getClass());
        this.color = color.cpy();
        acceleration = new Vector2();

        appliedForce = new Vector2();
        appliedForceOverTime = new Vector2();
        timeAppliedForceOverTime = 0f;
        timeToApplyForceOverTime = 0f;

        dragForce = new Vector2();
        dragCoefficient = 0.25f;
        totalAppliedForce = new Vector2();
        totalForce = new Vector2();

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyType.DynamicBody;
        body = getWorld().createBody(bodyDef);
        body.setUserData(this);

        CircleShape shape = new CircleShape();
        shape.setRadius(radius);
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        fixtureDef.density = density;
        fixtureDef.friction = friction;
        fixtureDef.restitution = restitution;
        fixture = body.createFixture(fixtureDef);

        shape.dispose();
    }

    public BlobBehavior getBehavior() {
        return behavior;
    }

    protected void setBehavior(BlobBehavior behavior) {
        behavior.setBehaver(this);
        this.behavior = behavior;
    }

    public Vector2 getAcceleration() {
        return acceleration;
    }

    protected void setAcceleration(Vector2 acceleration) {
        acceleration.x = acceleration.x;
        acceleration.y = acceleration.y;
    }

    protected void setAcceleration(float x, float y) {
        acceleration.x = x;
        acceleration.y = y;
    }

    public Vector2 getAppliedForce() {
        return appliedForce;
    }

    public Vector2 getAppliedForceOverTime() {
        return appliedForceOverTime;
    }

    public Vector2 getTotalAppliedForce() {
        return totalAppliedForce;
    }

    public Vector2 getTotalForce() {
        return totalForce;
    }

    public Vector2 getDragForce() {
        return dragForce;
    }

    public float getAngle() {
        return body.getAngle();
    }

    protected void setAngle(float angle) {
        body.setTransform(getPosition(), angle);
    }

    public float getAngularVelocity() {
        return body.getAngularVelocity();
    }

    protected void setAngularVelocity(float angularVelocity) {
        body.setAngularVelocity(angularVelocity);
    }

    public Body getBody() {
        return body;
    }

    public Color getColor() {
        return color;
    }

    protected void setColor(Color color) {
        this.color.r = color.r;
        this.color.g = color.g;
        this.color.b = color.b;
    }

    public float getDiameter() {
        return getRadius() * 2f;
    }

    public Fixture getFixture() {
        return fixture;
    }

    public float getMass() {
        return MathUtil.mass(getRadius(), fixture.getDensity());
    }

    public void setMass(float mass, boolean keepDensity) {
        if (keepDensity) {
            float newRadius = MathUtil.radius(mass, fixture.getDensity());
            fixture.getShape().setRadius(newRadius);
        } else {
            float newDensity = MathUtil.density(mass, getRadius());
            fixture.setDensity(newDensity);
            fixture.getBody().resetMassData();
        }
    }

    public String getName() {
        return name;
    }

    public Vector2 getPosition() {
        return body.getPosition();
    }

    protected void setPosition(Vector2 position) {
        setPosition(position.x, position.y);
    }

    protected void setPosition(float x, float y) {
        body.setTransform(x, y, getAngle());
    }

    public float getRadius() {
        return getShape().getRadius();
    }

    public Shape getShape() {
        return fixture.getShape();
    }

    public Vector2 getVelocity() {
        return body.getLinearVelocity();
    }

    protected void setVelocity(Vector2 velocity) {
        setVelocity(velocity.x, velocity.y);
    }

    protected void setVelocity(float x, float y) {
        body.setLinearVelocity(x, y);
    }

    public float viewDistance() {
        return Math.min(MAX_VIEW_DISTANCE, getDiameter() * VIEW_DISTANCE_MODIFIER);
    }

    public float maxApplyForce() {
        return MAX_APPLY_FORCE;
    }

    public float maxApplyForceOverTime() {
        float modifier = 1f / MathUtils.log(MathUtils.E * 20f, getMass());
        return modifier * APPLY_FORCE_OVER_TIME_FORCE_PER_MASS * getMass();
    }

    public float maxApplyForceOverTimeImproved() {
        return maxApplyForceOverTime() * APPLY_FORCE_OVER_TIME_IMPROVED_MODIFIER;
    }

    public void applyForce(Vector2 value) {
        applyForce(value.x, value.y);
    }

    public void applyForce(float x, float forceY) {
        appliedForce.set(x, forceY);
    }

    public void applyForceOverTime(Vector2 force) {
        applyForceOverTime(force.x, force.y);
    }

    public void applyForceOverTime(Vector2 force, float time) {
        applyForceOverTime(force.x, force.y, time);
    }

    public void applyForceOverTime(float x, float y) {
        applyForceOverTime(x, y, APPLY_FORCE_OVER_TIME_TIME);
    }

    public void applyForceOverTime(float x, float y, float time) {
        appliedForceOverTime.set(x, y);
        timeToApplyForceOverTime = time;
        timeAppliedForceOverTime = 0f;
    }

    public boolean applyingForceOverTime() {
        return timeToApplyForceOverTime > 0f;
    }

    public void update(float delta, float mapDragDensity, Array<Blob> blobs, Color mapBackgroundColor) {
        behavior.update(delta, blobs, mapBackgroundColor);
        updateAppliedForceOverTime(delta);
        updateDragForce(mapDragDensity);
        updateTotalAppliedForce();
        updateTotalForce();
        updateAcceleration();
        updateVelocity(delta);
    }

    protected void updateAppliedForceOverTime(float delta) {
        //Check if it's time to stop applying force over time!
        if (applyingForceOverTime()) {
            if (timeAppliedForceOverTime >= timeToApplyForceOverTime) {
                timeAppliedForceOverTime = 0f;
                timeToApplyForceOverTime = 0f;
                appliedForceOverTime.set(0f, 0f);
            } else {
                float timeLeftToApplyForce = timeToApplyForceOverTime - timeAppliedForceOverTime;
                //scale applied force over time for timeLeftToApplyForce
                if (timeLeftToApplyForce < delta) {
                    float scalar = timeLeftToApplyForce / delta;
                    appliedForceOverTime.scl(scalar);
                }
                timeAppliedForceOverTime += delta;
            }
        }
    }

    protected void updateDragForce(float dragDensity) {
        dragForce.set(getVelocity().x, getVelocity().y).nor().scl(-1);
        float area = MathUtil.area(getRadius());
        float speed = getVelocity().len();
        float dragAmount = 0.5f * dragCoefficient * dragDensity * area * (float) Math.pow(speed, 2);
        dragForce.setLength(dragAmount);
    }

    protected void updateTotalForce() {
        Vector2 totalAppliedForce = getTotalAppliedForce();
        Vector2 dragForce = getDragForce();
        totalForce.set(totalAppliedForce.x + dragForce.x, totalAppliedForce.y + dragForce.y);
    }

    protected void updateTotalAppliedForce() {
        float x = getAppliedForce().x + getAppliedForceOverTime().x;
        float y = getAppliedForce().y + getAppliedForceOverTime().y;
        totalAppliedForce.set(x, y);
    }

    protected void updateAcceleration() {
        assert (getMass() != 0f);
        setAcceleration(getTotalForce().x / getMass(), getTotalForce().y / getMass());
    }

    protected void updateVelocity(float delta) {
        Vector2 oldVelocity = getVelocity();
        float newVelocityX = oldVelocity.x + acceleration.x * delta;
        float newVelocityY = oldVelocity.y + acceleration.y * delta;
        setVelocity(newVelocityX, newVelocityY);
    }

    /**
     * Merge blob with this blob
     *
     * @param blob the blob to merge with this
     */
    public void merge(Blob blob) {
        this.setMass(getMass() + blob.getMass(), true);
        setColor(blob.getColor());
    }

    private void mergeColors(Blob blob) {
        float thisModifier = 1 - OTHER_BLOB_MERGE_COLOR_MODIFIER;
        float otherModifier = OTHER_BLOB_MERGE_COLOR_MODIFIER;
        color.r = (color.r * getMass() * thisModifier + blob.getColor().r * blob.getMass() * otherModifier) / (getMass() * thisModifier + blob.getMass() * otherModifier);
        color.g = (color.g * getMass() * thisModifier + blob.getColor().g * blob.getMass() * otherModifier) / (getMass() * thisModifier + blob.getMass() * otherModifier);
        color.b = (color.b * getMass() * thisModifier + blob.getColor().b * blob.getMass() * otherModifier) / (getMass() * thisModifier + blob.getMass() * otherModifier);
    }

    @Override
    public void dispose() {
        getWorld().destroyBody(getBody());
        body.setUserData(null);
        body = null;
    }
}