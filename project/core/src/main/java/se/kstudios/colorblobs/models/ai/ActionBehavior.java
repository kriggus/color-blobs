package se.kstudios.colorblobs.models.ai;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.utils.Array;

import java.util.Random;

import se.kstudios.colorblobs.models.Blob;

public abstract class ActionBehavior extends BlobBehavior {

    private final Random random;
    private final float takeActionProbIncreaseModifier;
    private final float takeActionCooldown;
    private float takeActionProbability;
    private float takeActionCooldownDelta;

    public ActionBehavior(float takeActionProbIncreaseModifier, float takeActionCooldown) {
        super();
        random = new Random();
        this.takeActionProbIncreaseModifier = takeActionProbIncreaseModifier;
        this.takeActionCooldown = takeActionCooldown;
        takeActionCooldownDelta = 0f;
        takeActionProbability = 0f;
    }

    @Override
    public void update(float delta, Array<Blob> blobs, Color backgroundColor) {
        this.blobs = blobs;
        this.backgroundColor = backgroundColor;
        if (takeActionTime(delta)) {
            takeAction();
            reset();
        }
    }

    protected abstract void takeAction();

    protected boolean takeActionTime(float delta) {
        takeActionCooldownDelta += delta;

        if (takeActionCooldownDelta < takeActionCooldown) {
            return false;
        }

        if (random.nextFloat() < takeActionProbability) {
            return true;
        } else {
            takeActionProbability += delta * takeActionProbIncreaseModifier;
            return false;
        }
    }

    protected void reset() {
        takeActionCooldownDelta = 0f;
        takeActionProbability = 0f;
    }
}

