package se.kstudios.colorblobs.views.renderers;

import java.util.Locale;

import com.badlogic.gdx.utils.Array;

import se.kstudios.colorblobs.models.Blob;
import se.kstudios.colorblobs.models.MemberTextPresentation;
import se.kstudios.colorblobs.views.RenderOptions;

public class BlobTextHudRenderer extends TextHudRenderer<Blob> {

	private Array<MemberTextPresentation<Blob>> memberPresentations;
	
	public BlobTextHudRenderer(float positionX, float positionY, float width, float height) {
		super("Player", positionX, positionY, width, height);
		
		memberPresentations = new Array<MemberTextPresentation<Blob>>();
		memberPresentations.add(
				new MemberTextPresentation<Blob>(b -> b.getName(), "Name"));
		memberPresentations.add(
				new MemberTextPresentation<Blob>(b -> String.format(Locale.US, TEXT_HUD_FLOAT_FORMAT, b.getMass()), "Mass"));
		memberPresentations.add(
				new MemberTextPresentation<Blob>(b -> b.getColor().toString(), "Color"));
		memberPresentations.add(
				new MemberTextPresentation<Blob>(b -> String.format(Locale.US, TEXT_HUD_FLOAT_FORMAT, b.getRadius()), "Radius"));
		memberPresentations.add(
				new MemberTextPresentation<Blob>(b -> String.format(Locale.US, TEXT_HUD_FLOAT_FORMAT, b.getAngle()), "Angle"));
		memberPresentations.add(
				new MemberTextPresentation<Blob>(
						b -> String.format(Locale.US, TEXT_HUD_VECTOR_FORMAT, b.getPosition().x, b.getPosition().y), "Pos"));
		memberPresentations.add(
				new MemberTextPresentation<Blob>(
						b -> String.format(Locale.US, TEXT_HUD_VECTOR_FORMAT, b.getTotalForce().x, b.getTotalForce().y), "Force"));
		memberPresentations.add(
				new MemberTextPresentation<Blob>(
						b -> String.format(Locale.US, TEXT_HUD_VECTOR_FORMAT, b.getAcceleration().x, b.getAcceleration().y), "Acc"));
		memberPresentations.add(
				new MemberTextPresentation<Blob>(
						b -> String.format(Locale.US, TEXT_HUD_VECTOR_FORMAT, b.getVelocity().x, b.getVelocity().y), "Vel"));		
	}

	@Override 
	public void render(Blob model) {
		if (RenderOptions.renderPlayerBlobHud) {
			defaultRender(model);
		}
	}

	@Override
	protected Array<MemberTextPresentation<Blob>> getMemberPresentations() {
		return memberPresentations;
	}
}
