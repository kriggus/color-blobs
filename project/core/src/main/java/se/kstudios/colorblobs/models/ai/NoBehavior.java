package se.kstudios.colorblobs.models.ai;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.utils.Array;

import se.kstudios.colorblobs.models.Blob;

public class NoBehavior extends BlobBehavior {

	public NoBehavior() {
		super();
	}

	@Override
	public Color color() {
		return Color.GRAY;
	}

	@Override
	public void update(float delta, Array<Blob> blobs, Color backgroundColor) {
		//Do nothing!
	}

}
