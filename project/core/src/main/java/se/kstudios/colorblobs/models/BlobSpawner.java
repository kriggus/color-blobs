package se.kstudios.colorblobs.models;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

import java.util.Random;

import se.kstudios.colorblobs.models.ai.EvadeHuntClosestBehavior;
import se.kstudios.colorblobs.utils.ColorUtil;
import se.kstudios.colorblobs.utils.MathUtil;

public class BlobSpawner {

    static final float DEFAULT_SPAWN_AREA_SIZE_MODIFIER = 2f;

    private Blob targetBlob;
    private Array<Blob> spawnedBlobs = new Array<>();
    private BlobBuilder blobBuilder = new BlobBuilder();
    private Random random = new Random();
    private float accumulatedDelta;
    private float deltaBetweenSpawns;
    private SpawnBlobSizeMode spawnBlobSizeMode;
    private SpawnTriggerMode spawnTriggerMode;
    private float minRelativeBlobSize;
    private float maxRelativeBlobSize;
    private Rectangle spawnArea = new Rectangle();
    private Rectangle lastSpawnArea = new Rectangle();
    private int spawnGeneration = 0;
    private int spawnsPerSpawnArea;
    private float spawnAreaSizeModifier;
    private boolean spawnInLastSpawnArea;
    private Vector2 lastSpawnPos = new Vector2();
    private Vector2 lastSpawnAreaBottomLeft = new Vector2();
    private Vector2 lastSpawnAreaTopLeft = new Vector2();
    private Vector2 lastSpawnAreaTopRight = new Vector2();
    private Vector2 lastSpawnAreaBottomRight = new Vector2();
    private Vector2 spawnAreaBottomLeft = new Vector2();
    private Vector2 spawnAreaTopLeft = new Vector2();
    private Vector2 spawnAreaTopRight = new Vector2();
    private Vector2 spawnAreaBottomRight = new Vector2();
    private Rectangle newSpawnArea1 = new Rectangle();
    private Rectangle newSpawnArea2 = new Rectangle();
    private Rectangle newSpawnArea3 = new Rectangle();
    private Rectangle newSpawnArea4 = new Rectangle();
    private Array<Rectangle> newSpawnAreas = new Array<>();

    public BlobSpawner(boolean spawnInLastSpawnArea,
                       float spawnAreaSizeModifier,
                       Blob targetBlob,
                       int meanSpawnsPerSpawnArea,
                       float minRelativeBlobSize,
                       float maxRelativeBlobSize,
                       SpawnBlobSizeMode spawnBlobSizeMode) {

        this.spawnInLastSpawnArea = spawnInLastSpawnArea;
        this.spawnTriggerMode = SpawnTriggerMode.distance;

        this.accumulatedDelta = 0f;
        this.spawnAreaSizeModifier = spawnAreaSizeModifier;
        this.targetBlob = targetBlob;
        this.spawnsPerSpawnArea = meanSpawnsPerSpawnArea;
        this.minRelativeBlobSize = minRelativeBlobSize;
        this.maxRelativeBlobSize = maxRelativeBlobSize;
        this.spawnBlobSizeMode = spawnBlobSizeMode;
    }

    public BlobSpawner(boolean spawnInLastSpawnArea,
                       float spawnAreaSizeModifier,
                       Blob targetBlob,
                       int meanSpawnsPerSpawnArea,
                       float minRelativeBlobSize,
                       float maxRelativeBlobSize,
                       SpawnBlobSizeMode spawnBlobSizeMode,
                       float deltaBetweenSpawns) {
        this.spawnInLastSpawnArea = spawnInLastSpawnArea;
        this.spawnTriggerMode = SpawnTriggerMode.time;
        this.deltaBetweenSpawns = deltaBetweenSpawns;
        this.accumulatedDelta = 0f;
        this.spawnAreaSizeModifier = spawnAreaSizeModifier;
        this.targetBlob = targetBlob;
        this.spawnsPerSpawnArea = meanSpawnsPerSpawnArea;
        this.minRelativeBlobSize = minRelativeBlobSize;
        this.maxRelativeBlobSize = maxRelativeBlobSize;
        this.spawnBlobSizeMode = spawnBlobSizeMode;
    }

    public int getSpawnGeneration() {
        return spawnGeneration;
    }

    public Rectangle getLastSpawnArea() {
        return lastSpawnArea;
    }

    public Rectangle getSpawnArea() {
        return spawnArea;
    }

    public Array<Blob> getSpawnedBlobs() {
        return spawnedBlobs;
    }

    public void spawn(float delta, Array<Blob> blobs, float spawnPosX, float spawnPosY, float spawnAreaWidth, float spawnAreaHeight, boolean force) {
        spawnedBlobs.clear();
        accumulatedDelta += delta;
        updateSpawnArea(spawnPosX, spawnPosY, spawnAreaWidth, spawnAreaHeight);
        if (triggerSpawn(spawnPosX, spawnPosY) || force) {
            updateBlobsToAdd(blobs);
            accumulatedDelta = 0f;
            lastSpawnPos.x = spawnPosX;
            lastSpawnPos.y = spawnPosY;
            lastSpawnArea.set(spawnArea);
            spawnGeneration++;
        }
    }

    private void updateSpawnArea(float spawnPosX, float spawnPosY, float spawnAreaWidth, float spawnAreaHeight) {
        float modifiedWidth = spawnAreaSizeModifier * spawnAreaWidth;
        float modifiedHeight = spawnAreaSizeModifier * spawnAreaHeight;
        float modifiedX = spawnPosX - modifiedWidth / 2f;
        float modifiedY = spawnPosY - modifiedHeight / 2f;
        spawnArea.set(modifiedX, modifiedY, modifiedWidth, modifiedHeight);
    }

    private boolean triggerSpawn(float spawnPosX, float spawnPosY) {
        switch (spawnTriggerMode) {
            case distance:
                return movedToTriggerSpawn(spawnPosX, spawnPosY);
            case time:
                return timeToTriggerSpawn();
            default:
                return false;
        }
    }

    private boolean timeToTriggerSpawn() {
        if (accumulatedDelta > deltaBetweenSpawns) {
            return true;
        }
        return false;
    }

    /**
     * Dependent on spawn area
     */
    private boolean movedToTriggerSpawn(float spawnPosX, float spawnPosY) {
        if (isFirstGeneration()) {
            return true;
        }

        float movedX = Math.abs(spawnPosX - lastSpawnPos.x);
        float movedY = Math.abs(spawnPosY - lastSpawnPos.y);
        float movedDistance = MathUtil.hypotenuse(movedX, movedY);
        return movedDistance >= distanceToTriggerSpawn();
    }

    private boolean isFirstGeneration() {
        return spawnGeneration == 0;
    }

    private float distanceToTriggerSpawn() {
        float modifiedLongestSide = Math.min(spawnArea.width, spawnArea.height);
        float screenLongestSide = modifiedLongestSide / spawnAreaSizeModifier;
        float diff = modifiedLongestSide - screenLongestSide;
        return diff / 4f;
    }

    private void updateBlobsToAdd(Array<Blob> blobs) {
        spawnedBlobs.clear();

        if (spawnInLastSpawnArea) {
            addBlobs(blobs, spawnsPerSpawnArea, spawnArea);
        } else {
            boolean movedInTwoDim = spawnArea.x != lastSpawnArea.x && spawnArea.y != lastSpawnArea.y;
            if (movedInTwoDim) {
                updateRectanglePoints(lastSpawnArea, lastSpawnAreaBottomLeft, lastSpawnAreaTopLeft, lastSpawnAreaTopRight, lastSpawnAreaBottomRight);
                updateRectanglePoints(spawnArea, spawnAreaBottomLeft, spawnAreaTopLeft, spawnAreaTopRight, spawnAreaBottomRight);
                updateNewSpawnAreaParts();
                float spawnAreaSize = spawnArea.area();
                for (Rectangle newSpawnArea : newSpawnAreas) {
                    float relativeSize = newSpawnArea.area() / spawnAreaSize;
                    int blobsToAddCount = Math.round(spawnsPerSpawnArea * relativeSize);
                    addBlobs(blobs, blobsToAddCount, newSpawnArea);
                }
            } else {
                //todo implement!
                System.out.println("one dim movement not yet implemented for blob spawner!");
            }
        }
    }

    private void addBlobs(Array<Blob> blobs, int count, Rectangle newSpawnArea) {
        for (int i = 0; i < count; i++) {
            float newBlobPosX = newSpawnArea.x + random.nextFloat() * newSpawnArea.width;
            float newBlobPosY = newSpawnArea.y + random.nextFloat() * newSpawnArea.height;
            float minNewBlobRadius = minNewBlobRadius(blobs);
            float maxNewBlobRadius = maxNewBlobRadius(blobs);
            float newBlobRadius = newBlobRadius(minNewBlobRadius, maxNewBlobRadius);
            if (!newBlobOverlapBlob(newBlobPosX, newBlobPosY, newBlobRadius, blobs)) {
                blobBuilder.setColor(ColorUtil.randomColor());
                blobBuilder.setRadius(newBlobRadius);
                blobBuilder.setPosition(newBlobPosX, newBlobPosY);
                blobBuilder.setBehavior(new EvadeHuntClosestBehavior(1f, 1f, 0.1f));
//                blobBuilder.setBehavior(new NoBehavior());
                spawnedBlobs.add(blobBuilder.build());
            }
        }
    }

    /**
     * Assumes spawn area and last spawn area and their points are updated.
     */
    private void updateNewSpawnAreaParts() {
        newSpawnAreas.clear();
        Vector2 point = lastPointInSpawnArea();
        if (point != null) {
            newSpawnArea1.x = spawnAreaBottomLeft.x;
            newSpawnArea1.y = spawnAreaBottomLeft.y;
            newSpawnArea1.width = point.x - spawnAreaBottomLeft.x;
            newSpawnArea1.height = point.y - spawnAreaBottomLeft.y;
            if (!lastSpawnArea.overlaps(newSpawnArea1)) {
                newSpawnAreas.add(newSpawnArea1);
            }

            newSpawnArea2.x = spawnAreaBottomLeft.x;
            newSpawnArea2.y = point.y;
            newSpawnArea2.width = point.x - spawnAreaBottomLeft.x;
            newSpawnArea2.height = spawnAreaTopLeft.y - point.y;
            if (!lastSpawnArea.overlaps(newSpawnArea2)) {
                newSpawnAreas.add(newSpawnArea2);
            }

            newSpawnArea3.x = point.x;
            newSpawnArea3.y = point.y;
            newSpawnArea3.width = spawnAreaTopRight.x - point.x;
            newSpawnArea3.height = spawnAreaTopRight.y - point.y;
            if (!lastSpawnArea.overlaps(newSpawnArea3)) {
                newSpawnAreas.add(newSpawnArea3);
            }

            newSpawnArea4.x = point.x;
            newSpawnArea4.y = spawnAreaBottomRight.y;
            newSpawnArea4.width = spawnAreaTopRight.x - point.x;
            newSpawnArea4.height = point.y - spawnAreaBottomRight.y;
            if (!lastSpawnArea.overlaps(newSpawnArea4)) {
                newSpawnAreas.add(newSpawnArea4);
            }
        }
    }

    /**
     * Assumes spawn area nad last spawn area points are updated.
     */
    private Vector2 lastPointInSpawnArea() {
        if (spawnArea.contains(lastSpawnAreaBottomLeft)) {
            return lastSpawnAreaBottomLeft;
        } else if (spawnArea.contains(lastSpawnAreaTopLeft)) {
            return lastSpawnAreaTopLeft;
        } else if (spawnArea.contains(lastSpawnAreaTopRight)) {
            return lastSpawnAreaTopRight;
        } else if (spawnArea.contains(lastSpawnAreaBottomRight)) {
            return lastSpawnAreaBottomRight;
        }
        return null;
    }

    private void updateRectanglePoints(Rectangle rect, Vector2 bottomLeft, Vector2 topLeft, Vector2 topRight, Vector2 bottomRight) {
        bottomLeft.x = rect.x;
        bottomLeft.y = rect.y;
        topLeft.x = rect.x;
        topLeft.y = rect.y + rect.height;
        topRight.x = rect.x + rect.width;
        topRight.y = rect.y + rect.height;
        bottomRight.x = rect.x + rect.width;
        bottomRight.y = rect.y;
    }

    private float newBlobRadius(float minRadius, float maxRadius) {
        return minRadius + random.nextFloat() * (maxRadius - minRadius);
    }

    private float maxNewBlobRadius(Array<Blob> blobs) {
        switch (spawnBlobSizeMode) {
            case relativeToMean:
                return maxRelativeBlobSize * meanBlobRadius(blobs);
            case relativeToTarget:
                return maxRelativeBlobSize * targetBlob.getRadius();
            default:
                throw new RuntimeException("SpawnBlobSizeMode not supported");
        }
    }

    private float minNewBlobRadius(Array<Blob> blobs) {
        switch (spawnBlobSizeMode) {
            case relativeToMean:
                return minRelativeBlobSize * meanBlobRadius(blobs);
            case relativeToTarget:
                return minRelativeBlobSize * targetBlob.getRadius();
            default:
                throw new RuntimeException("SpawnBlobSizeMode not supported");
        }
    }

    private float meanBlobRadius(Array<Blob> blobs) {
        if (blobs.size == 0) {
            return 0f;
        }
        float totalRadius = 0f;
        for (Blob blob : blobs) {
            totalRadius += blob.getRadius();
        }
        return totalRadius / blobs.size;
    }

    private boolean newBlobOverlapBlob(float x, float y, float radius, Array<Blob> blobs) {
        for (Blob blob : blobs) {
            if (MathUtil.circleOverlapBlob(x, y, radius, blob)) {
                return true;
            }
        }
        return false;
    }

    public enum SpawnBlobSizeMode {relativeToTarget, relativeToMean}

    public enum SpawnTriggerMode {distance, time}
}