package se.kstudios.colorblobs.views.renderers;

import java.util.Locale;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;

import se.kstudios.colorblobs.controllers.GameplayCamera;
import se.kstudios.colorblobs.models.Blob;
import se.kstudios.colorblobs.models.MemberTextPresentation;
import se.kstudios.colorblobs.utils.RenderUtil;
import se.kstudios.colorblobs.views.RenderOptions;

public class BlobMemberTextsRenderer extends Renderer<Blob> implements Disposable {
	
	private static final Color BLOB_MEBMER_TEXT_FOREGROUND_COLOR = Color.WHITE;
	private static final float BLOB_MEMBER_TEXT_FORGROUND_SCALING = 0.75f;
	private static final String BLOB_MEMBER_TEXT_VECTOR_FORAMT = "%.0f,%.0f";
	private static final String BLOB_MEMBER_TEXT_FLOAT_FORMAT = "%.0f";
	
	private SpriteBatch textSpriteBatch;
	private Matrix4 cameraCombinedMatrix; 
	private Matrix4 projectionMatrix;
	private float cameraRotationDeg;
	private BitmapFont font;
	private StringBuilder dispTextBuilder;
	private Array<MemberTextPresentation<Blob>> memberPresentations;
	
	public BlobMemberTextsRenderer() {
		textSpriteBatch = new SpriteBatch();
		cameraCombinedMatrix = new Matrix4();
		projectionMatrix = new Matrix4();
		
		font = new BitmapFont();
		font.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
		font.getData().setScale(BLOB_MEMBER_TEXT_FORGROUND_SCALING);
		font.setColor(BLOB_MEBMER_TEXT_FOREGROUND_COLOR);
		
		dispTextBuilder = new StringBuilder();
		
		memberPresentations = new Array<MemberTextPresentation<Blob>>();
		memberPresentations.add(
				new MemberTextPresentation<Blob>(b -> b.getName(), "n"));
		memberPresentations.add(
				new MemberTextPresentation<Blob>(b -> String.format(Locale.US, BLOB_MEMBER_TEXT_FLOAT_FORMAT, b.getMass()), "m"));
		memberPresentations.add(
				new MemberTextPresentation<Blob>(b -> b.getColor().toString(), "c"));
		memberPresentations.add(
				new MemberTextPresentation<Blob>(
						b -> String.format(Locale.US, BLOB_MEMBER_TEXT_VECTOR_FORAMT, b.getPosition().x, b.getPosition().y), "p"));
		memberPresentations.add(
				new MemberTextPresentation<Blob>(
						b -> String.format(Locale.US, BLOB_MEMBER_TEXT_VECTOR_FORAMT, b.getTotalForce().x, b.getTotalForce().y), "f"));
		memberPresentations.add(
				new MemberTextPresentation<Blob>(
						b -> String.format(Locale.US, BLOB_MEMBER_TEXT_VECTOR_FORAMT, b.getVelocity().x, b.getVelocity().y), "v"));
	}


    public boolean renderBlobMemberTexts() {
        return RenderOptions.renderBlobMemberTexts;
    }

	@Override
	public void update(GameplayCamera camera) {
		cameraCombinedMatrix.set(camera.combined);
		cameraRotationDeg = camera.getRotationDegrees();
		font.getData().setScale(Math.max(0.1f, BLOB_MEMBER_TEXT_FORGROUND_SCALING * camera.zoom));
	}
	
	@Override 
	public void render(Blob model)  {
		if (renderBlobMemberTexts()) {
			defaultRender(model);
		}
	}
	
	@Override
	protected void defaultRender(Blob model) {
		updateDispText(model);
		
		float screenRadius = RenderUtil.getPixelCoordinate(model.getRadius());
		float screenX = RenderUtil.getPixelCoordinate(model.getPosition().x);
		float screenY = RenderUtil.getPixelCoordinate(model.getPosition().y);
		
		projectionMatrix.set(cameraCombinedMatrix);
		projectionMatrix.translate(screenX, screenY, 0f);
		projectionMatrix.rotate(0f, 0f, 1f, -cameraRotationDeg);
		projectionMatrix.translate(-screenX, -screenY, 0f);
		textSpriteBatch.setProjectionMatrix(projectionMatrix);
		
		textSpriteBatch.begin();
		
		font.draw(textSpriteBatch, dispTextBuilder.toString(), screenX - screenRadius, screenY - screenRadius);
		
		textSpriteBatch.end();
	}

	private void updateDispText(Blob model) {
		dispTextBuilder.setLength(0);
		boolean first = true;
		for(MemberTextPresentation<Blob> memberPresentation : memberPresentations) {
			if (!first) {
				dispTextBuilder.append(' ');
			}
			dispTextBuilder.append(memberPresentation.getText("%s=%s", model));
			first = false;
		}
	}
	
	@Override
	protected void debugRender(Blob model) {
		throw new UnsupportedOperationException("debugRender not supported for " + this.getClass().getName());
	}

	@Override
	public void dispose() {
		textSpriteBatch.dispose();
		font.dispose();
	}
}
