package se.kstudios.colorblobs.models.ai;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.utils.Array;

import se.kstudios.colorblobs.models.Blob;

public abstract class BlobBehavior {

    protected final float DETECT_COLOR_DEVIATION = 0.2f;

    protected Color backgroundColor;
    protected Blob behaver;
    protected Array<Blob> blobs;

    public BlobBehavior() {
        blobs = new Array<>();
    }

    public void setBehaver(Blob behaver) {
        this.behaver = behaver;
    }

    public abstract Color color();

    public abstract void update(float delta, Array<Blob> blobs, Color backgroundColor);

    protected boolean detectBlob(Blob blob, Color backgroundColor) {
        Color blobColor = blob.getColor();
        float totalDiff = difference(blobColor.r, backgroundColor.r) + difference(blobColor.g, backgroundColor.g) + difference(blobColor.b, backgroundColor.b);
        return totalDiff >= DETECT_COLOR_DEVIATION;
    }

    private float difference(float a, float b) {
        return Math.abs(a - b);
    }
}
