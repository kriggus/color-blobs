package se.kstudios.colorblobs.views.renderers;

import java.util.Locale;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;

import se.kstudios.colorblobs.controllers.screens.GameplayScreen;
import se.kstudios.colorblobs.models.MemberTextPresentation;
import se.kstudios.colorblobs.views.RenderOptions;

public class ScreenTextHudRenderer extends TextHudRenderer<GameplayScreen> {

	private Array<MemberTextPresentation<GameplayScreen>> memberPresentations;
	
	public ScreenTextHudRenderer(float positionX, float positionY, float width, float height) {
		super("Screen", positionX, positionY, width, height);
		
		memberPresentations = new Array<>();
		memberPresentations.add(
				new MemberTextPresentation<>(
				        s -> Integer.toString(Gdx.graphics.getFramesPerSecond()), "FPS"));
		memberPresentations.add(
				new MemberTextPresentation<>(s -> String.format(Locale.US, TEXT_HUD_FLOAT_FORMAT,
                        s.getUpdateMapSteppingManager().getTotalDelta()), "Time"));
		memberPresentations.add(
				new MemberTextPresentation<>(
				        s -> Integer.toString(s.getMap().getSpawnGeneration()), "Spawn Gen"));
	}

	@Override
	public void render(GameplayScreen model) {
		if (RenderOptions.renderScreenHud) {
			defaultRender(model);
		}
	}

	@Override
	protected Array<MemberTextPresentation<GameplayScreen>> getMemberPresentations() {
		return memberPresentations;
	}
}