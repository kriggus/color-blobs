package se.kstudios.colorblobs.controllers;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector3;

import se.kstudios.colorblobs.utils.MathUtil;
import se.kstudios.colorblobs.utils.RenderUtil;
import se.kstudios.colorblobs.views.RenderOptions;

public class GameplayCamera extends OrthographicCamera {

    private static final float FAR = 1002f;
    private static final float NEAR = 1f;
    private static final float POSITION_Z = 2f;
    private static final float LOOSE_MODE_INTERPOLATION_MODIFIER = 3f;
    private static final float MAX_ZOOM_NUMBER_OF_TARGET_RADIUS_ON_SCREEN = 50f;

    public CameraMode cameraMode;
    private Vector3 targetScreenPosition;
    private Polygon viewportPolygon;
    private float[] transformedViewportCoordX;
    private float[] transformedViewportCoordY;
    private float maxZoom;
    private float minZoom;

    public GameplayCamera(float positionX, float positionY) {
        super(RenderUtil.getViewportWidth(), RenderUtil.getViewportHeight());
        position.x = positionX;
        position.y = positionY;
        position.z = POSITION_Z;
        targetScreenPosition = new Vector3(position.x, position.y, position.z);
        viewportPolygon = new Polygon(new float[]{
                -viewportWidth / 2f, -viewportHeight / 2f,
                -viewportWidth / 2f, viewportHeight / 2f,
                viewportWidth / 2f, viewportHeight / 2f,
                viewportWidth / 2f, -viewportHeight / 2f});
        transformedViewportCoordX = new float[]{0f, 0f, 0f, 0f};
        transformedViewportCoordY = new float[]{0f, 0f, 0f, 0f};

        cameraMode = CameraMode.LOOSE;
        near = NEAR;
        far = FAR;

        minZoom = 1f;
        maxZoom = 5f;
    }

//    public void zoom(float amount) {
//        zoom = Math.max(getMinZoom(), Math.min(getMaxZoom(), zoom + amount));
//    }

    public float getMinZoom() {
        return minZoom;
    }

    public float getMaxZoom() {
        return maxZoom;
    }

    public float getRotationDegrees() {
        return getRotationRadians() * MathUtils.radiansToDegrees;
    }

    public float getRotationRadians() {
        return (float) Math.atan2(up.x, up.y);
    }

    public float[] getTransformedViewportVertices() {
        return viewportPolygon.getTransformedVertices();
    }

    public float getMinTransformedViewportCoordX() {
        return MathUtil.min(transformedViewportCoordX);
    }

    public float getMinTransformedViewportCoordY() {
        return MathUtil.min(transformedViewportCoordY);
    }

    public float getMaxTransformedViewportCoordX() {
        return MathUtil.max(transformedViewportCoordX);
    }

    public float getMaxTransformedViewportCoordY() {
        return MathUtil.max(transformedViewportCoordY);
    }

    public void update(float delta, float targetScreenPosX, float targetScreenPosY, float targetScreenRadius) {
        updatePosition(delta, targetScreenPosX, targetScreenPosY);
        updateMaxZoom(targetScreenRadius);
        updateZoom(delta);
        updateViewportWorldPolygon();
        super.update();
    }

    private void updateMaxZoom(float targetScreenRadius) {
        float screenWidth = RenderUtil.getViewportWidth();
        float screenHeight = RenderUtil.getViewportHeight();
        float shortestSide = Math.min(screenHeight, screenWidth);
        maxZoom = (MAX_ZOOM_NUMBER_OF_TARGET_RADIUS_ON_SCREEN * targetScreenRadius * 2f) / shortestSide;
    }

    private void updateZoom(float delta) {
        zoom = Math.max(0, zoom);

        if (!RenderOptions.debugRender) {
            float diffAbove = zoom - getMaxZoom();
            if (diffAbove > 0f) {
                zoom = zoom - diffAbove * Math.min(1f, delta * 3f);
            }

            float diffBelow = getMinZoom() - zoom;
            if (diffBelow > 0f) {
                zoom = zoom + diffBelow * delta;
            }
        }
    }

    private void updatePosition(float delta, float targetScreenPositionX, float targetScreenPositionY) {
        targetScreenPosition.x = targetScreenPositionX;
        targetScreenPosition.y = targetScreenPositionY;

        switch (cameraMode) {
            case LOOSE:
                position.lerp(targetScreenPosition, LOOSE_MODE_INTERPOLATION_MODIFIER * delta);
                position.z = POSITION_Z;
                break;
            case TIGHT:
            default:
                position.x = targetScreenPosition.x;
                position.y = targetScreenPosition.y;
                position.z = POSITION_Z;
                break;
        }
    }

    private void updateViewportWorldPolygon() {
        viewportPolygon.setPosition(position.x, position.y);
        viewportPolygon.setRotation(-getRotationDegrees());
        viewportPolygon.setScale(zoom, zoom);

        float[] transformedViewportVertices = viewportPolygon.getTransformedVertices();

        transformedViewportCoordX[0] = transformedViewportVertices[0];
        transformedViewportCoordX[1] = transformedViewportVertices[2];
        transformedViewportCoordX[2] = transformedViewportVertices[4];
        transformedViewportCoordX[3] = transformedViewportVertices[6];

        transformedViewportCoordY[0] = transformedViewportVertices[1];
        transformedViewportCoordY[1] = transformedViewportVertices[3];
        transformedViewportCoordY[2] = transformedViewportVertices[5];
        transformedViewportCoordY[3] = transformedViewportVertices[7];
    }

    public enum CameraMode {TIGHT, LOOSE}
}
