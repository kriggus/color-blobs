package se.kstudios.colorblobs.models;

@FunctionalInterface
public interface MemberTextGetter<T> {
	String getText(T object);
}
