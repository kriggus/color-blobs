package se.kstudios.colorblobs.models.ai;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

import se.kstudios.colorblobs.models.Blob;

/**
 * Behavior for blob movement then blob try to evade closest blob within view distance bigger than itself.
 *
 * @author Kristofer Carlsson
 */

public class EvadeClosestBiggerBehavior extends ActionBehavior {

    private final float sizeDeviation;
    private Blob closestThreat;
    private float closestThreatDistance;
    private Vector2 applyForce;
    private RandomBehavior randomBehavior;

    public EvadeClosestBiggerBehavior(float takeActionProbIncreaseModifier, float takeActionCooldown, float sizeDeviation) {
        super(takeActionProbIncreaseModifier, takeActionCooldown);
        this.sizeDeviation = sizeDeviation;
        applyForce = new Vector2();
        randomBehavior = new RandomBehavior(takeActionProbIncreaseModifier, takeActionCooldown);
    }

    @Override
    public void update(float delta, Array<Blob> blobs, Color backgroundColor) {
        super.update(delta, blobs, backgroundColor);
    }

    @Override
    public void setBehaver(Blob behaver) {
        super.setBehaver(behaver);
        randomBehavior.setBehaver(behaver);
    }

    @Override
    public Color color() {
        return Color.TEAL;
    }

    protected boolean hasClosestThreat() {
        return closestThreat != null;
    }

    protected Blob getClosestThreat() {
        return closestThreat;
    }

    protected float getClosestThreatDistance() {
        return closestThreatDistance;
    }

    @Override
    protected void takeAction() {
        findClosestThreat();
        if (hasClosestThreat()) {
            float behaverX = behaver.getPosition().x;
            float behaverY = behaver.getPosition().y;
            float threatX = closestThreat.getPosition().x;
            float threatY = closestThreat.getPosition().y;
            applyForce.set(behaverX - threatX, behaverY - threatY);
            applyForce.nor();
            applyForce.scl(behaver.maxApplyForceOverTime());
            behaver.applyForceOverTime(applyForce);
        } else {
            randomBehavior.takeAction();
        }
    }

    @Override
    protected void reset() {
        super.reset();
        randomBehavior.reset();
    }

    protected void findClosestThreat() {
        closestThreat = null;
        closestThreatDistance = Float.MAX_VALUE;
        for (int i = 0; i < blobs.size; i++) {
            Blob blob = blobs.get(i);
            if (blob != behaver && detectBlob(blob, backgroundColor) && isThreat(blob)) {
                float distance = behaver.getPosition().dst(blob.getPosition());
                if (distance < closestThreatDistance && distance <= behaver.viewDistance()) {
                    closestThreat = blob;
                    closestThreatDistance = distance;
                }
            }
        }
    }

    private boolean isThreat(Blob blob) {
        return behaver.getMass() < blob.getMass() * (1f - sizeDeviation);
    }
}
