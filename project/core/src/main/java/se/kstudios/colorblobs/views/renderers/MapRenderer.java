package se.kstudios.colorblobs.views.renderers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.utils.Disposable;

import se.kstudios.colorblobs.controllers.GameplayCamera;
import se.kstudios.colorblobs.models.Map;
import se.kstudios.colorblobs.utils.RenderUtil;

public class MapRenderer extends Renderer<Map> implements Disposable {
	
	private static final int MAP_DEBUG_LINE_SPACING = 100;
	private static final Color MAP_MINOR_LINE_COLOR = new Color(0x9f9f9f33);
	private static final Color MAP_DEBUG_MINOR_LINE_COLOR = Color.DARK_GRAY;
	private static final Color MAP_DEBUG_MAJOR_LINE_COLOR = Color.LIGHT_GRAY;
	
	private ShapeRenderer shapeRenderer;
	private ShapeRenderer backgroundColorRenderer;
	private float lineSpacing;
	private float viewportMaxX;
	private float viewportMinX;
	private float viewportMaxY;
	private float viewportMinY;
	
	private float viewportWidth;
	private float viewportHeight;
	
	public MapRenderer() {
		shapeRenderer = new ShapeRenderer();
		backgroundColorRenderer = new ShapeRenderer();
		lineSpacing = RenderUtil.getPixelCoordinate(MAP_DEBUG_LINE_SPACING);
	}
	
	@Override
	public void update(GameplayCamera camera) {
		viewportWidth = camera.viewportWidth;
		viewportHeight = camera.viewportHeight;

		viewportMaxX = camera.getMaxTransformedViewportCoordX();
		viewportMinX = camera.getMinTransformedViewportCoordX();
		viewportMaxY = camera.getMaxTransformedViewportCoordY();
		viewportMinY = camera.getMinTransformedViewportCoordY();
		
		shapeRenderer.setProjectionMatrix(camera.combined);
	}

	@Override
	public void dispose() {
		shapeRenderer.dispose();
		backgroundColorRenderer.dispose();
	}
	
	@Override
	protected void defaultRender(Map model) {
		renderBackground(model);
		renderCoordSystemWeakLines();
	}

	@Override
	protected void debugRender(Map model) {
		renderCoordSystemWeakLines();
		renderCoordSystemAxes();
	}

	private void renderBackground(Map model) {
		backgroundColorRenderer.setColor(model.getBackgroundColor());
		backgroundColorRenderer.begin(ShapeType.Filled);
		backgroundColorRenderer.rect(0, 0, viewportWidth, viewportHeight);
		backgroundColorRenderer.end();
	}

	private void renderCoordSystemAxes() {
		shapeRenderer.setColor(MAP_DEBUG_MAJOR_LINE_COLOR);
		shapeRenderer.begin(ShapeType.Line);
		shapeRenderer.line(viewportMinX, 0f, viewportMaxX, 0f);
		shapeRenderer.line(0f, viewportMinY, 0f, viewportMaxY);
		shapeRenderer.end();
	}
	
	private void renderCoordSystemWeakLines() {
		Gdx.gl.glEnable(GL30.GL_BLEND);
		shapeRenderer.setColor(MAP_MINOR_LINE_COLOR);
		shapeRenderer.begin(ShapeType.Line);
		
		//render vertical lines
		float x = startCoordinate(viewportMinX);
		while(x < viewportMaxX) {
			shapeRenderer.line(x, viewportMinY, x, viewportMaxY);
			x += lineSpacing;
		}
		
		//render horizontal lines
		float y = startCoordinate(viewportMinY);
		while(y < viewportMaxY) {
			shapeRenderer.line(viewportMinX, y, viewportMaxX, y);
			y += lineSpacing;
		}
		
		shapeRenderer.end();
		Gdx.gl.glDisable(GL30.GL_BLEND);
	}
	
	private float startCoordinate(float minCoord) {
		if (minCoord < 0f) {
			return minCoord + Math.abs(minCoord) % lineSpacing;	
		}
		else {			
			return minCoord + (lineSpacing - minCoord % lineSpacing);
		}
	}
}
