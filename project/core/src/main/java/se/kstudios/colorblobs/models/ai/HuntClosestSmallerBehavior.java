package se.kstudios.colorblobs.models.ai;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

import se.kstudios.colorblobs.models.Blob;

/**
 * Behavior for blob movement then blob try to hunt closest blob within view distance smaller than itself.
 *
 * @author Kristofer Carlsson
 */
public class HuntClosestSmallerBehavior extends ActionBehavior {


    private final float sizeDeviation;
    private Blob closestPrey;
    private float closestPreyDistance;
    private Vector2 applyForce;
    private RandomBehavior randomBehavior;

    public HuntClosestSmallerBehavior(float takeActionProbIncreaseModifier, float takeActionCooldown, float sizeDeviation) {
        super(takeActionProbIncreaseModifier, takeActionCooldown);
        this.sizeDeviation = sizeDeviation;
        applyForce = new Vector2();
        randomBehavior = new RandomBehavior(takeActionProbIncreaseModifier, takeActionCooldown);
    }

    @Override
    public void update(float delta, Array<Blob> blobs, Color backgroundColor) {
        super.update(delta, blobs, backgroundColor);
    }

    @Override
    public void setBehaver(Blob behaver) {
        super.setBehaver(behaver);
        randomBehavior.setBehaver(behaver);
    }

    @Override
    public Color color() {
        return Color.CORAL;
    }

    protected boolean hasClosestPrey() {
        return closestPrey != null;
    }

    protected Blob getClosestPrey() {
        return closestPrey;
    }

    protected float getClosestPreyDistance() {
        return closestPreyDistance;
    }

    @Override
    protected void takeAction() {
        findClosestPrey();
        if (hasClosestPrey()) {
            float behaverX = behaver.getPosition().x;
            float behaverY = behaver.getPosition().y;
            float preyX = closestPrey.getPosition().x;
            float preyY = closestPrey.getPosition().y;
            applyForce.set(preyX - behaverX, preyY - behaverY);
            applyForce.nor();
            applyForce.scl(behaver.maxApplyForceOverTime());
            behaver.applyForceOverTime(applyForce);
        } else {
            randomBehavior.takeAction();
        }
    }

    @Override
    protected void reset() {
        super.reset();
        randomBehavior.reset();
    }

    protected void findClosestPrey() {
        closestPrey = null;
        closestPreyDistance = Float.MAX_VALUE;
        for (int i = 0; i < blobs.size; i++) {
            Blob blob = blobs.get(i);
            if (blob != behaver && detectBlob(blob, backgroundColor) && isPrey(blob)) {
                float distance = behaver.getPosition().dst(blob.getPosition());
                if (distance < closestPreyDistance && distance <= behaver.viewDistance()) {
                    closestPrey = blob;
                    closestPreyDistance = distance;
                }
            }
        }
    }

    private boolean isPrey(Blob blob) {
        return behaver.getMass() > blob.getMass() * (1f - sizeDeviation);
    }
}
