package se.kstudios.colorblobs.models;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;

import se.kstudios.colorblobs.models.ai.BlobBehavior;
import se.kstudios.colorblobs.models.ai.NoBehavior;

public class BlobBuilder {
	
	private final float defaultRadius= 10f;
	private final float defaultDensity = 5.5f;
	private final float defaultFriction = 0.1f;
	private final float defaultRestitution = 0.5f;
	private final Color defaultColor = new Color(0f, 0f, 1f, 1f);
	private final Vector2 defaultPosition = new Vector2(0f, 0f);
	private final Vector2 defaultVelocity = new Vector2(0f, 0f);
	private final BlobBehavior defaultBehavior = new NoBehavior();
	
	private float radius;
	private float density; 
	private float friction;
	private float restitution;
	private Color color;
	private Vector2 position;
	private Vector2 velocity;
	private BlobBehavior behavior;
	
	public BlobBuilder() {
		position = new Vector2();
		velocity = new Vector2();
		setDefault();
	}

	public void setDefault() {
		setDefaultRadius();
		setDefaultDensity();
		setDefaultFriction();
		setDefaultRestitution();
		setDefaultColor();
		setDefaultPosition();
		setDefaultVelocity();
		setDefaultBehavior();
	}
	
	public void setRadius(float radius) {
		this.radius = radius;
	}
	
	public void setDensity(float density) {
		this.density = density;
	}
	
	public void setFriction(float friction) {
		this.friction = friction;
	}
	
	public void setRestitution(float restitution) {
		this.restitution = restitution;
	}
	
	public void setColor(Color color) {
		this.color = color;
	}
	
	public void setPosition(float positionX, float positionY) {
		position.x = positionX;
		position.y = positionY;
	}

	public void setVelocity(float x, float y) {
		velocity.set(x, y);
	}
	
	public void setBehavior(BlobBehavior behavior) {
		this.behavior = behavior;
	}
	
	public void setDefaultRadius() {
		setRadius(defaultRadius);
	}
	
	public void setDefaultDensity() {
		setDensity(defaultDensity);
	}
	
	public void setDefaultFriction() {
		setFriction(defaultFriction);
	}
	
	public void setDefaultRestitution() {
		setRestitution(defaultRestitution);
	}
	
	public void setDefaultColor() {
		setColor(defaultColor);
	}

	public void setDefaultPosition() {
		setPosition(defaultPosition.x, defaultPosition.y);
	}
	
	public void setDefaultVelocity() {
		setVelocity(defaultVelocity.x, defaultVelocity.y);
	}
	
	private void setDefaultBehavior() {
		behavior = defaultBehavior;
	}
	
	public Blob build() {
		Blob blob =  new Blob(radius, density, friction, restitution, color);
		blob.setPosition(position);
		blob.setVelocity(velocity);
		blob.setBehavior(behavior);
		return blob;
	}
}
