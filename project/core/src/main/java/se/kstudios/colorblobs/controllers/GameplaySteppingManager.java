package se.kstudios.colorblobs.controllers;

public class GameplaySteppingManager {

	private float stepLength;
	private float accumulatedDelta;
	private float totalDelta;
	
	public GameplaySteppingManager(float stepLength) {
		this.stepLength = stepLength;
		accumulatedDelta = 0f;
		totalDelta = 0f;
	}
	
	public void addDelta(float delta) {
		accumulatedDelta += delta;
		totalDelta += delta;
	}
	
	public boolean hasStep() {
		return accumulatedDelta >= stepLength;
	}
	
	public float nextStep() {
		if (hasStep()) {
			accumulatedDelta -= stepLength;
			return stepLength;
		}
		else {
			return 0f;
		}
	}
	
	public float getTotalDelta() {
		return totalDelta;
	}
}
