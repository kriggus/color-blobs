package se.kstudios.colorblobs.models.ai;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.utils.Array;

import se.kstudios.colorblobs.models.Blob;

public class EvadeHuntClosestBehavior extends ActionBehavior {

    private EvadeClosestBiggerBehavior evadeBehavior;
    private HuntClosestSmallerBehavior huntBehavior;

    public EvadeHuntClosestBehavior(float takeActionProbIncreaseModifier, float takeActionCooldown, float sizeDeviation) {
        super(takeActionProbIncreaseModifier, takeActionCooldown);
        evadeBehavior = new EvadeClosestBiggerBehavior(takeActionProbIncreaseModifier, takeActionCooldown, sizeDeviation);
        huntBehavior = new HuntClosestSmallerBehavior(takeActionProbIncreaseModifier, takeActionCooldown, sizeDeviation);
    }

    @Override
    public void update(float delta, Array<Blob> blobs, Color backgroundColor) {
        evadeBehavior.blobs = blobs;
        evadeBehavior.backgroundColor = backgroundColor;
        huntBehavior.blobs = blobs;
        huntBehavior.backgroundColor = backgroundColor;
        super.update(delta, blobs, backgroundColor);
    }

    @Override
    public void setBehaver(Blob behaver) {
        super.setBehaver(behaver);
        evadeBehavior.setBehaver(behaver);
        huntBehavior.setBehaver(behaver);
    }

    @Override
    protected void takeAction() {
        evadeBehavior.findClosestThreat();
        if (evadeBehavior.hasClosestThreat()) {
            evadeBehavior.takeAction();
        } else {
            huntBehavior.findClosestPrey();
            huntBehavior.takeAction();
        }
    }

    @Override
    protected void reset() {
        super.reset();
        evadeBehavior.reset();
        huntBehavior.reset();
    }

    @Override
    public Color color() {
        return Color.GOLDENROD;
    }
}
