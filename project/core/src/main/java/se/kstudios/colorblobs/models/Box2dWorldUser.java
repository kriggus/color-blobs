package se.kstudios.colorblobs.models;

import com.badlogic.gdx.physics.box2d.World;

public class Box2dWorldUser {
	
	public Box2dWorldManager getWorldManager() {
		return Box2dWorldManager.getInstance();
	}
	
	public World getWorld() {
		return getWorldManager().getWorld();
	}
}
