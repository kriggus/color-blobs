package se.kstudios.colorblobs.views.renderers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;

import se.kstudios.colorblobs.controllers.GameplayCamera;
import se.kstudios.colorblobs.controllers.screens.GameplayScreen;
import se.kstudios.colorblobs.models.Blob;
import se.kstudios.colorblobs.utils.RenderUtil;

public class GameplayScreenRenderer extends Renderer<GameplayScreen> implements Disposable {
	
	private MapRenderer mapRenderer;
	private BlobRenderer blobRenderer;
	private BlobMemberTextsRenderer blobMemberTextRenderer;
	private ScreenTextHudRenderer screenTextHudRenderer;
	private CameraTextHudRenderer cameraTextHudRenderer;
	private MapTextHudRenderer mapTextHudRenderer;
	private BlobTextHudRenderer playerTextHudRenderer;
	private ShapeRenderer shapeRenderer;
	
	public GameplayScreenRenderer() {
		mapRenderer = new MapRenderer();
		blobRenderer = new BlobRenderer();
		blobMemberTextRenderer = new BlobMemberTextsRenderer();
		screenTextHudRenderer = new ScreenTextHudRenderer(0, Gdx.graphics.getHeight(), 160, 110);
		cameraTextHudRenderer = new CameraTextHudRenderer(0, Gdx.graphics.getHeight() - 120, 160, 110);
		mapTextHudRenderer = new MapTextHudRenderer(0, Gdx.graphics.getHeight() - 120 - 120, 160, 90);
		playerTextHudRenderer = new BlobTextHudRenderer(0, Gdx.graphics.getHeight() - 120 - 120 - 100, 160, 215);
		shapeRenderer = new ShapeRenderer();
	}

	@Override
	public void update(GameplayCamera camera) {
		mapRenderer.update(camera);
		blobRenderer.update(camera);
		blobMemberTextRenderer.update(camera);
		screenTextHudRenderer.update(camera);
		cameraTextHudRenderer.update(camera);
		mapTextHudRenderer.update(camera);
		playerTextHudRenderer.update(camera);
		shapeRenderer.setProjectionMatrix(camera.combined);
	}
	
	@Override
	public void dispose() {
		mapRenderer.dispose();
		blobRenderer.dispose();
		blobMemberTextRenderer.dispose();
		screenTextHudRenderer.dispose();
		cameraTextHudRenderer.dispose();
		mapTextHudRenderer.dispose();
		playerTextHudRenderer.dispose();
		shapeRenderer.dispose();
	}
	
	@Override
	protected void defaultRender(GameplayScreen model) {
		clearBuffers();
		renderMap(model);
		renderBlobs(model);
		renderHUDs(model);	
	}

	@Override
	protected void debugRender(GameplayScreen model) {
		clearBuffers();
		renderSpawnArea(model.getMap().getSpawnArea(), new Color(1f, 0.75f, 085f, 0.3f));
		renderSpawnArea(model.getMap().getLastSpawnArea(), new Color(0.75f, 1f, 085f, 0.3f));
		renderMap(model);
		renderBlobs(model);
		renderHUDs(model);
	}
	
	private void clearBuffers() {
		Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT | GL30.GL_DEPTH_BUFFER_BIT);
		Gdx.gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	}
	
	private void renderMap(GameplayScreen screen) {
		mapRenderer.render(screen.getMap());
	}
	
	private void renderSpawnArea(Rectangle spawnArea, Color color) {
		Gdx.gl.glEnable(GL30.GL_BLEND);
		shapeRenderer.setColor(color);
		shapeRenderer.begin(ShapeType.Filled);
		float x = RenderUtil.getPixelCoordinate(spawnArea.x);
		float y = RenderUtil.getPixelCoordinate(spawnArea.y);
		float width = RenderUtil.getPixelCoordinate(spawnArea.width);
		float height = RenderUtil.getPixelCoordinate(spawnArea.height);
		shapeRenderer.rect(x, y, width, height);
		shapeRenderer.end();
		Gdx.gl.glDisable(GL30.GL_BLEND);
	}

	private void renderBlobs(GameplayScreen screen) {
		blobRenderer.begin();
		Array<Blob> blobs = screen.getMap().getBlobs();
		Blob playerBlob = screen.getMap().getPlayerBlob();
		for (Blob blob : blobs) {
			blobRenderer.render(blob);
		}
		blobRenderer.render(playerBlob);
        blobRenderer.end();

        if (blobMemberTextRenderer.renderBlobMemberTexts()) {
            for (Blob blob : blobs) {
                blobMemberTextRenderer.render(blob);
            }
            blobMemberTextRenderer.render(playerBlob);
        }
    }
	
	private void renderHUDs(GameplayScreen screen) {
		Blob playerBlob = screen.getMap().getPlayerBlob();
		mapTextHudRenderer.render(screen.getMap());
		cameraTextHudRenderer.render(screen.getCamera());
		playerTextHudRenderer.render(playerBlob);
		screenTextHudRenderer.render(screen);
	}
	
}
