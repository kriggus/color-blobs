package se.kstudios.colorblobs.models;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Disposable;

/**
 * Manages the current instance of the box2d world.
 * 
 * @author Kristofer Carlsson
 *
 */
public class Box2dWorldManager implements Disposable {

	private static Box2dWorldManager worldManager;
	
	private World world;
	private int worldObjectCounter;
	
	private Box2dWorldManager() { 
		worldObjectCounter = -1;
	}
	
	public static Box2dWorldManager getInstance() {
		if (worldManager == null) {
			worldManager = new Box2dWorldManager();
		}
		return worldManager;
	}
	
	public boolean hasWorld() {
		return world != null;
	}
	
	public World getWorld() {
		return world;
	}
	
	/**
	 * Creates a new world, resets world object counter and disposes the old world.
	 */
	public void createWorld() {
		if (hasWorld()) {
			disposeWorld();
		}
		world = new World(new Vector2(0,0), true);
		worldObjectCounter = 0;
	}
	
	/**
	 * Disposes the world.
	 */
	public void disposeWorld() {
		if (hasWorld()) {			
			world.dispose();
			world = null;
		}
	}
	
	/**
	 * Disposes the world and sets the world manager instance to null.
	 */
	@Override
	public void dispose() {
		disposeWorld();
		worldManager = null;
	}
	
	public int getNextObjectNumber() {
		if (hasWorld()) {
			return worldObjectCounter++;
		}
		else {
			return -1;
		}
	}
	
	public <T> String getNextObjectName(Class<T> classVar) {
		return String.format("%s_%s", classVar.getSimpleName(), getNextObjectNumber());
	}
}