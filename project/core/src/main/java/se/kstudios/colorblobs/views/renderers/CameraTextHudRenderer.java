package se.kstudios.colorblobs.views.renderers;

import java.util.Locale;

import com.badlogic.gdx.utils.Array;

import se.kstudios.colorblobs.controllers.GameplayCamera;
import se.kstudios.colorblobs.models.MemberTextPresentation;
import se.kstudios.colorblobs.views.RenderOptions;

public class CameraTextHudRenderer extends TextHudRenderer<GameplayCamera> {
	
	private Array<MemberTextPresentation<GameplayCamera>> memberPresentations;
	
	public CameraTextHudRenderer(float positionX, float positionY, float width, float height) {
		super("Camera", positionX, positionY, width, height);
		
		memberPresentations = new Array<MemberTextPresentation<GameplayCamera>>();
		memberPresentations.add(
				new MemberTextPresentation<GameplayCamera>(
						c -> String.format(Locale.US, TEXT_HUD_VECTOR_FORMAT, c.position.x, c.position.y), "Pos"));
		memberPresentations.add(
				new MemberTextPresentation<GameplayCamera>(
						c -> String.format(Locale.US, TEXT_HUD_FLOAT_FORMAT, c.zoom), "Zoom"));
		memberPresentations.add(new MemberTextPresentation<GameplayCamera>(
						c -> String.format(Locale.US, TEXT_HUD_FLOAT_FORMAT, c.getRotationDegrees()), "Rotation"));	
	}

	@Override
	public void render(GameplayCamera model)  {
		if (RenderOptions.renderCameraHud) {
			defaultRender(model);
		}
	};
	
	
	@Override
	protected Array<MemberTextPresentation<GameplayCamera>> getMemberPresentations() {
		return memberPresentations;
	}
}
