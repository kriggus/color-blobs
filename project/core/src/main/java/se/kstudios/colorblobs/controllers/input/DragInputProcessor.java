package se.kstudios.colorblobs.controllers.input;

import com.badlogic.gdx.math.Vector2;

import se.kstudios.colorblobs.controllers.GameplayCamera;
import se.kstudios.colorblobs.models.Blob;

public class DragInputProcessor extends GameplayInputProcessor {

	public enum DragMode { Discrete, Gradual };
	
	private DragMode dragMode; 
	private float lengthForMinAppliedForce;
	private float lengthForMaxAppliedForce;
	private Vector2 appliedForce;
	
	public DragInputProcessor(Blob targetBlob, GameplayCamera camera, int screenWidth, int screenHeight) {
		super(targetBlob, camera, screenWidth, screenHeight);
		dragMode = DragMode.Discrete;
		lengthForMinAppliedForce = Math.round(Math.min(screenWidth, screenHeight) / 20);
		lengthForMaxAppliedForce = Math.round(Math.min(screenWidth, screenHeight) / 2);		
		appliedForce = new Vector2();
	}
	
	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		super.touchDown(screenX, screenY, pointer, button);
		if (pointerCount() > 1) {
			appliedForce.set(0f, 0f);
			targetBlob.applyForce(appliedForce);
		}
		return true;
	}
	
	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		super.touchUp(screenX, screenY, pointer, button);
		if (pointerCount() == 0) {
			appliedForce.set(0f, 0f);
			targetBlob.applyForce(appliedForce);
		}
		return true;
	}
	
	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		super.touchDragged(screenX, screenY, pointer);
		if (pointerCount() == 1) {
			updateAppliedForce(screenX, screenY);
			targetBlob.applyForce(appliedForce);
		}
		return true;
	}
	
	@Override
	public void update(float delta) {
		//Do nothing
	}
	
	private void updateAppliedForce(int screenX, int screenY) {
		assert(pointerCount() == 1);
		
		Vector2 pointerStartPos = pointerStartPosition(0);
		float dragX =  -(screenX - pointerStartPos.x);
		float dragY = screenY - pointerStartPos.y;
		float dragLength = (float) Math.sqrt((Math.pow(dragX, 2) + Math.pow(dragY, 2)));

		if (dragLength < lengthForMinAppliedForce) {
			appliedForce.set(0f, 0f);
		}
		else {
			appliedForce.x = dragX;
			appliedForce.y = dragY;
			appliedForce.nor();
			if (dragMode == DragMode.Discrete) {
				appliedForce.setLength(targetBlob.maxApplyForce());
			}
			else if (dragMode == DragMode.Gradual) {
				float amountOfAppliedForce = Math.min(1, (dragLength / lengthForMaxAppliedForce));
				appliedForce.setLength(amountOfAppliedForce * targetBlob.maxApplyForce());
			}
			appliedForce.rotate(-camera.getRotationDegrees()); // compensate for that the model never rotates.
		}
	}
}
