package se.kstudios.colorblobs.models;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;

import se.kstudios.colorblobs.utils.RenderUtil;

public class Map extends Box2dWorldUser implements Disposable {

    private static final int BOX2D_VELOCITY_ITERATIONS = 4;
    private static final int BOX2D_POSITION_ITERATIONS = 2;
    private static final float DEFAULT_MAP_DRAG_DENSITY = 0.3f;
    private static final int ACTIVE_AREA_GRID_SIZE = 16;
    private static final float BACKGROUND_CHANGE_SPEED = 1f / 120f;

    private ColorChangeable backgroundChanger;
    private Array<Blob> blobs;
    private Blob playerBlob;
    private Array<Array<Array<Blob>>> activeAreaGrid;
    private float mapDragDensity;
    private Array<Blob> blobsToDispose;
    private BlobSpawner smallBlobsSpawner;
    private BlobSpawner bigBlobsSpawner;

    private boolean forceRespawn = false;

    public Map(Blob playerBlob) {
        this.playerBlob = playerBlob;
        mapDragDensity = DEFAULT_MAP_DRAG_DENSITY;

        Color startBackgroundColor = new Color(0f, 1f, 0f, 1f);
        backgroundChanger = new ColorComponentChanger(startBackgroundColor, BACKGROUND_CHANGE_SPEED);

        smallBlobsSpawner = new BlobSpawner(true, 1f, playerBlob, 1, 0.3f, 0.67f, BlobSpawner.SpawnBlobSizeMode.relativeToTarget, 1f);
        bigBlobsSpawner = new BlobSpawner(false, 2f, playerBlob, 150, 0.5f, 2f, BlobSpawner.SpawnBlobSizeMode.relativeToTarget);

        blobsToDispose = new Array<>();

        blobs = new Array<>();
        blobs.add(playerBlob);

        viewableBlobs = new Array<>();

        activeAreaGrid = new Array<>();
        for (int i = 0; i < ACTIVE_AREA_GRID_SIZE; i++) {
            activeAreaGrid.add(new Array<>());
            for (int j = 0; j < ACTIVE_AREA_GRID_SIZE; j++) {
                activeAreaGrid.get(i).add(new Array<>());
            }
        }

        getWorld().setContactListener(new BlobContactListener(this, playerBlob));
    }

    public Color getBackgroundColor() {
        return backgroundChanger.getColor();
    }

    public Blob getPlayerBlob() {
        return playerBlob;
    }

    public Array<Blob> getBlobs() {
        return blobs;
    }

    public Rectangle getSpawnArea() {
        return bigBlobsSpawner.getSpawnArea();
    }

    public Rectangle getLastSpawnArea() {
        return bigBlobsSpawner.getLastSpawnArea();
    }

    public int getSpawnGeneration() {
        return bigBlobsSpawner.getSpawnGeneration();
    }

    public void update(float delta, float maxZoom) {
        backgroundChanger.update(delta);
        removeBlobsOutsideActiveArea();
        spawnBlobsInActiveArea(delta, maxZoom);
        updateActiveAreaGrid();
        updateBlobs(delta);
        updateBox2dWorld(delta);
        disposeBlobs();
    }

    private void removeBlobsOutsideActiveArea() {
        for (int i = 0; i < blobs.size; i++) {
            if (!getSpawnArea().contains(blobs.get(i).getPosition())) {
                blobsToDispose.add(blobs.removeIndex(i));
                i--;
            }
        }
    }

    private void spawnBlobsInActiveArea(float delta, float maxZoom) {
        float width = RenderUtil.getViewportModelWidth();
        float height = RenderUtil.getViewportModelHeight();
        float longestSide = Math.max(width, height);
        longestSide = longestSide * maxZoom; //to avoid regeneration of blobs when zooming out and in.

        Vector2 pos = playerBlob.getPosition();

        smallBlobsSpawner.spawn(delta, blobs, pos.x, pos.y, longestSide, longestSide, false);
        bigBlobsSpawner.spawn(delta, blobs, pos.x, pos.y, longestSide, longestSide, forceRespawn);

        if (forceRespawn) {
            forceRespawn = false;
        }

        addBlobs(smallBlobsSpawner.getSpawnedBlobs());
        addBlobs(bigBlobsSpawner.getSpawnedBlobs());
    }

    private void addBlobs(Iterable<Blob> blobs) {
        for (Blob blob : blobs) {
            this.blobs.add(blob);
        }
    }

    void mergeBlobs(Blob bigBlob, Blob smallBlob) {
        if (playerBlob == smallBlob) {
            //TODO handle game over
            return;
        }
        if (playerBlob == bigBlob) {
            forceRespawn = true;
        }
        if (blobs.removeValue(smallBlob, true)) {
            blobsToDispose.add(smallBlob);
            bigBlob.merge(smallBlob);
        }
    }

    private void disposeBlobs() {
        for (Blob blob : blobsToDispose) {
            blob.dispose();
        }
        blobsToDispose.clear();
    }

    @Override
    public void dispose() {
        for (Blob blob : blobs) {
            blob.dispose();
        }
        blobs.clear();

        for(Blob blob : blobsToDispose) {
            blob.dispose();
        }
        blobsToDispose.clear();
    }

    private void updateBox2dWorld(float delta) {
        getWorld().step(delta, BOX2D_VELOCITY_ITERATIONS, BOX2D_POSITION_ITERATIONS);
    }

    private void updateBlobs(float delta) {
        for (int x = 0; x < activeAreaGrid.size; x++){
            for (int y = 0; y < activeAreaGrid.get(x).size; y++) {
                for(Blob blob : activeAreaGrid.get(x).get(y)) {
                    updateViewableBlobs(blob, x, y);
                    blob.update(delta, mapDragDensity, viewableBlobs, getBackgroundColor());
                }
            }
        }
    }

    private Array<Blob> viewableBlobs;
    private void updateViewableBlobs(Blob blob, int blobActiveAreaGridX, int blobActiveAreaGridY) {
        viewableBlobs.clear();
        float cellSize = activeAreaGridCellLength();
        int cellSizes = (int) Math.ceil((double) (blob.viewDistance() / cellSize));
        for (int x = 0; x < cellSizes + 1; x++) {
            for (int y = 0; y < cellSizes + 1; y++) {
                int activeAreaGridX = blobActiveAreaGridX + x;
                int activeAreaGridY = blobActiveAreaGridY + y;
                boolean isInActiveAreaGridX = activeAreaGridX < ACTIVE_AREA_GRID_SIZE - 1;
                boolean isInActiveAreaGridY = activeAreaGridY < ACTIVE_AREA_GRID_SIZE - 1;
                if (isInActiveAreaGridX && isInActiveAreaGridY) {
                    viewableBlobs.addAll(activeAreaGrid.get(activeAreaGridX).get(activeAreaGridY));
                }
            }
        }
    }

    private void updateActiveAreaGrid() {
        for (int i = 0; i < ACTIVE_AREA_GRID_SIZE; i++) {
            for (int j = 0; j < ACTIVE_AREA_GRID_SIZE; j++) {
                activeAreaGrid.get(i).get(j).clear();
            }
        }

        for (int i = 0; i < blobs.size; i++) {
            Blob blob = blobs.get(i);
            int indexX = calcActiveAreaGridIndexX(blob);
            int indexY = calcActiveAreaGridIndexY(blob);
            activeAreaGrid.get(indexX).get(indexY).add(blob);
        }
    }

    private int calcActiveAreaGridIndexX(Blob blob) {
        float gridPosX = blob.getPosition().x - getSpawnArea().x;
        return (int) Math.min(ACTIVE_AREA_GRID_SIZE - 1, gridPosX / activeAreaGridCellLength());
    }

    private int calcActiveAreaGridIndexY(Blob blob) {
        float gridPosY = blob.getPosition().y - getSpawnArea().y;
        return (int) Math.min(ACTIVE_AREA_GRID_SIZE - 1, gridPosY / activeAreaGridCellLength());
    }

    private float activeAreaGridCellLength() {
        return getSpawnArea().width / ACTIVE_AREA_GRID_SIZE;
    }
}

