package se.kstudios.colorblobs.views;

public class RenderOptions {
	public static boolean debugRender = false;
	public static boolean renderScreenHud = true;
	public static boolean renderCameraHud = false;
	public static boolean renderMapHud = false;
	public static boolean renderPlayerBlobHud = false;
	public static boolean renderBlobMemberTexts = false;
}
