package se.kstudios.colorblobs.views.renderers;

import com.badlogic.gdx.utils.Array;

import se.kstudios.colorblobs.models.Map;
import se.kstudios.colorblobs.models.MemberTextPresentation;
import se.kstudios.colorblobs.views.RenderOptions;

public class MapTextHudRenderer extends TextHudRenderer<Map> {

	private Array<MemberTextPresentation<Map>> memberPresentations;
	
	public MapTextHudRenderer(float positionX, float positionY, float width, float height) {
		super("Map", positionX, positionY, width, height);
		
		memberPresentations = new Array<MemberTextPresentation<Map>>();
		memberPresentations.add(
				new MemberTextPresentation<Map>(
						b -> b.getBackgroundColor().toString(), "Background"));
		memberPresentations.add(
				new MemberTextPresentation<Map>(
						b -> b.getPlayerBlob() == null ? Integer.toString(b.getBlobs().size) : Integer.toString((b.getBlobs().size + 1)), 
						"Blobs"));
	}

	@Override
	public void render(Map model) {
		if (RenderOptions.renderMapHud) {
			defaultRender(model);
		}
	}

	@Override
	protected Array<MemberTextPresentation<Map>> getMemberPresentations() {
		return memberPresentations;
	}
}
