package se.kstudios.colorblobs.views.renderers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;

import se.kstudios.colorblobs.controllers.GameplayCamera;
import se.kstudios.colorblobs.models.MemberTextPresentation;

public abstract class TextHudRenderer<T> extends Renderer<T> implements Disposable {
	
	static final Color TEXT_HUD_FOREGROUND_COLOR = Color.WHITE;
	static final float TEXT_HUD_FOREGROUND_SCALING = 1f;
	static final Color TEXT_HUD_BACKGROUND_COLOR = new Color(0f, 0f, 0f, 0.25f);
	static final String TEXT_HUD_VECTOR_FORMAT = "%.0f,%.0f";
	static final String TEXT_HUD_FLOAT_FORMAT = "%.2f";
	
	private float positionX;
	private float positionY;
	private float width;
	private float height;
	private String name;
	private SpriteBatch spriteBatch;
	private ShapeRenderer shapeRenderer;
	private BitmapFont font;
	private StringBuilder dispTextBuilder;
	
	public TextHudRenderer(String name, float positionX, float positionY, float width, float height) {
		this.positionX = positionX;
		this.positionY = positionY;
		this.width = width;
		this.height = height;
		this.name = name;
		
		spriteBatch = new SpriteBatch();
		shapeRenderer = new ShapeRenderer();
		
		font = new BitmapFont();
		font.getData().setScale(TEXT_HUD_FOREGROUND_SCALING);
		font.setColor(TEXT_HUD_FOREGROUND_COLOR);
		shapeRenderer.setColor(TEXT_HUD_BACKGROUND_COLOR);

		dispTextBuilder = new StringBuilder();
	}
	
	protected abstract Array<MemberTextPresentation<T>> getMemberPresentations();
	
	@Override
	public void update(GameplayCamera camera) { }

	@Override
	protected void defaultRender(T model) {
		defaultRenderBackground();		
		defaultRenderForeground(model);
	}

	private void defaultRenderBackground() {
		Gdx.gl.glEnable(GL30.GL_BLEND);
		shapeRenderer.begin(ShapeType.Filled);
		shapeRenderer.rect(positionX, positionY, width, -height);
		shapeRenderer.end();
		Gdx.gl.glDisable(GL30.GL_BLEND);
	}

	private void defaultRenderForeground(T model) {
		updateDisplayText(model);

		float posX = positionX + 10;
		float posY = positionY - 10;

		spriteBatch.begin();
		font.draw(spriteBatch, dispTextBuilder.toString(), posX, posY);
		spriteBatch.end();
	}

	private void updateDisplayText(T model) {
		dispTextBuilder.setLength(0);
		dispTextBuilder.append(name);
		dispTextBuilder.append("\n");
		dispTextBuilder.append("\n");
		boolean first = true;
		Array<MemberTextPresentation<T>> memberPresentations = getMemberPresentations();
		for(MemberTextPresentation<T> memberPresentation : memberPresentations) {
			if (!first) {
				dispTextBuilder.append("\n");
			}
			dispTextBuilder.append(memberPresentation.getText("%s: %s", model));
			first = false;
		}
	}

	@Override
	protected void debugRender(T model) {
		throw new UnsupportedOperationException("debugRender not supported for " + this.getClass().getName());
	}
	
	@Override
	public void dispose() {
		spriteBatch.dispose();
		shapeRenderer.dispose();
		font.dispose();
	}
}
