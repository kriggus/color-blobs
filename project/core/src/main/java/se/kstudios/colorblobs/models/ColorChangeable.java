package se.kstudios.colorblobs.models;

import com.badlogic.gdx.graphics.Color;

public interface ColorChangeable {
	
	Color getColor();
	void update(float delta);	
}
