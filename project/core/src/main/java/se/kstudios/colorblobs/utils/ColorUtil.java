package se.kstudios.colorblobs.utils;

import com.badlogic.gdx.graphics.Color;

import java.util.EnumSet;
import java.util.Random;
import java.util.function.Predicate;

public class ColorUtil {

	public enum ColorComponent { Red, Green, Blue }
	
	private static Random random = new Random();
	
	public static EnumSet<ColorComponent> getColorComponents(Predicate<Float> predicate, Color color) {
		EnumSet<ColorComponent> result = EnumSet.noneOf(ColorComponent.class);
		if (predicate.test(color.r))
			result.add(ColorComponent.Red);
		if (predicate.test(color.g))
			result.add(ColorComponent.Green);
		if (predicate.test(color.b))
			result.add(ColorComponent.Blue);
		return result;
	}
	
	public static ColorComponent getRandomColorComponent(ColorComponent forbidden) {
		ColorComponent[] possible;
		switch (forbidden) {
		case Red:
			possible = new ColorComponent[] {ColorComponent.Green, ColorComponent.Blue};
			break;
		case Green:
			possible = new ColorComponent[] {ColorComponent.Red, ColorComponent.Blue};
			break;
		case Blue:
			possible = new ColorComponent[] {ColorComponent.Green, ColorComponent.Red};
			break;
		default:
			throw new UnsupportedOperationException("Color component value is not supported");
		}
		int i = random.nextInt(2);
		return possible[i];
	}

	public static ColorComponent getRandomColorComponent() {
		ColorComponent[] possible = new ColorComponent[] { ColorComponent.Red, ColorComponent.Green, ColorComponent.Blue };
		return possible[random.nextInt(3)];
	}
	
	public static Color randomColor() {
		ColorComponent changingComponent = getRandomColorComponent();
		ColorComponent fullComponent = getRandomColorComponent(changingComponent);
		ColorComponent emptyComponent = EnumSet.complementOf(EnumSet.of(changingComponent, fullComponent)).iterator().next();
		
		float[] colorValues = new float[3];
		colorValues[changingComponent.ordinal()] = MathUtil.round(random.nextFloat(), 1);
		colorValues[fullComponent.ordinal()] = 1f;
		colorValues[emptyComponent.ordinal()] = 0f;
		
		return new Color(colorValues[0], colorValues[1], colorValues[2], 1f);
	}
	
	public static boolean isEmpty(float channelValue) {
		return channelValue == 0f;
	}

	public static boolean isFull(float channelValue) {
		return channelValue == 1f;
	}
}
