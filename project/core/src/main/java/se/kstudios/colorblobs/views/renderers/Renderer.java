package se.kstudios.colorblobs.views.renderers;

import se.kstudios.colorblobs.controllers.GameplayCamera;
import se.kstudios.colorblobs.views.RenderOptions;

public abstract class Renderer<T> {
	
	public abstract void update(GameplayCamera camera);
	
	public void render(T model) {
		if (RenderOptions.debugRender) {
			debugRender(model);
		}
		else {
			defaultRender(model);
		}
	}
	
	protected abstract void defaultRender(T model);
		
	protected abstract void debugRender(T model);
}
