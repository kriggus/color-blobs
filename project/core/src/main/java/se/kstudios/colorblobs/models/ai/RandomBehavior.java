package se.kstudios.colorblobs.models.ai;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;

import java.util.Random;

/**
 * Behavior for blob movement lacking strategy whatsoever.
 *
 * @author Kristofer Carlsson
 */
public class RandomBehavior extends ActionBehavior {

    private final Random random;
    private Vector2 applyForce;

    public RandomBehavior(float takeActionProbIncreaseModifier, float takeActionCooldown) {
        super(takeActionProbIncreaseModifier, takeActionCooldown);
        random = new Random();
        applyForce = new Vector2();
    }

    @Override
    public Color color() {
        return Color.PURPLE;
    }

    @Override
    public void takeAction() {
        applyForce.set(1f, 0f);
        applyForce.rotate(random.nextFloat() * 360f);
        applyForce.scl(behaver.maxApplyForceOverTime());
        behaver.applyForceOverTime(applyForce);
    }
}
