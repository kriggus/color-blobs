package se.kstudios.colorblobs.controllers.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;

import se.kstudios.colorblobs.controllers.GameplayCamera;
import se.kstudios.colorblobs.controllers.GameplaySteppingManager;
import se.kstudios.colorblobs.controllers.input.ClickInputProcessor;
import se.kstudios.colorblobs.controllers.input.GameplayInputProcessor;
import se.kstudios.colorblobs.models.Blob;
import se.kstudios.colorblobs.models.BlobBuilder;
import se.kstudios.colorblobs.models.Box2dWorldManager;
import se.kstudios.colorblobs.models.Map;
import se.kstudios.colorblobs.utils.RenderUtil;
import se.kstudios.colorblobs.views.renderers.GameplayScreenRenderer;

public class GameplayScreen implements Screen {

    private Map map;
    private GameplayCamera camera;
    private GameplaySteppingManager steppingManager;
    private GameplayInputProcessor inputProcessor;
    private GameplayScreenRenderer screenRenderer;

    @Override
    public void show() {
        initializeModels();
        initializeControllers();
        initializeViews();
    }

    public Map getMap() {
        return map;
    }

    public GameplayCamera getCamera() {
        return camera;
    }

    public GameplaySteppingManager getUpdateMapSteppingManager() {
        return steppingManager;
    }

    private void initializeModels() {
        //Box2d
        Box2dWorldManager.getInstance().createWorld();

        //player blob
        BlobBuilder blobBuilder = new BlobBuilder();
        blobBuilder.setColor(new Color(0f, 0f, 1f, 1f));
        blobBuilder.setRadius(10f);
        blobBuilder.setPosition(0f, 0f);
        Blob playerBlob = blobBuilder.build();

        //map
        map = new Map(playerBlob);
    }

    private void initializeControllers() {
        steppingManager = new GameplaySteppingManager(1f / RenderUtil.TARGET_FPS_RATE);

        //camera
        Vector2 targetPos = map.getPlayerBlob().getPosition();
        float targetScreenPosX = RenderUtil.getPixelCoordinate(targetPos.x);
        float targetScreenPosY = RenderUtil.getPixelCoordinate(targetPos.y);
        camera = new GameplayCamera(targetScreenPosX, targetScreenPosY);

        //input processor
        inputProcessor = new ClickInputProcessor(map.getPlayerBlob(), camera, RenderUtil.getViewportWidth(), RenderUtil.getViewportHeight());
        Gdx.input.setInputProcessor(inputProcessor);
    }

    private void initializeViews() {
        screenRenderer = new GameplayScreenRenderer();
    }

    @Override
    public void render(float delta) {
        updateControllers(delta);
        updateModel();
        realRender();
    }

    private void updateModel() {
        while (steppingManager.hasStep()) {
            map.update(steppingManager.nextStep(), camera.getMaxZoom());
        }
    }

    private void updateControllers(float delta) {
        steppingManager.addDelta(delta);
        inputProcessor.update(delta);
        updateCamera(delta);
    }

    private void updateCamera(float delta) {
        Blob targetBlob = map.getPlayerBlob();
        float targetScreenRadius = RenderUtil.getPixelCoordinate(targetBlob.getRadius());
        float targetScreenPosX = RenderUtil.getPixelCoordinate(targetBlob.getPosition().x);
        float targetScreenPosY = RenderUtil.getPixelCoordinate(targetBlob.getPosition().y);
        camera.update(delta, targetScreenPosX, targetScreenPosY, targetScreenRadius);
    }

    private void realRender() {
        screenRenderer.update(camera);
        screenRenderer.render(this);
    }

    @Override
    public void resize(int width, int height) {
        // TODO Auto-generated method stub

    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub

    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub

    }

    @Override
    public void hide() {
        // TODO Auto-generated method stub
    }

    @Override
    public void dispose() {
        map.dispose();
        screenRenderer.dispose();
        Box2dWorldManager.getInstance().dispose();
    }
}
