package se.kstudios.colorblobs.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;

public class RenderUtil {
	
	/**
	 * Pixels per model unit.
	 */
	public static final int MODEL_TO_PIXEL_RATIO = 4;
	public static final int TARGET_FPS_RATE = 60;
	
	public static float getPixelCoordinate(float modelCoordinate) {
		return modelCoordinate * MODEL_TO_PIXEL_RATIO;
	}
	
	public static Vector2 getPixelPosition(Vector2 position) {
		Vector2 screenPos = new Vector2();
		screenPos.x = getPixelCoordinate(position.x);
		screenPos.y = getPixelCoordinate(position.y);
		return screenPos;
	}
	
	public static float getModelCoordinate(float screenCoordinate) {
		return screenCoordinate / MODEL_TO_PIXEL_RATIO;
	}
	
	public static Vector2 getModelPosition(Vector2 position) {
		Vector2 modelPos = new Vector2();
		modelPos.x = getModelCoordinate(position.x);
		modelPos.y = getModelCoordinate(position.y);
		return modelPos;
	}

	public static float getViewportModelWidth() {
		return getModelCoordinate(getViewportWidth());
	}

	public static float getViewportModelHeight() {
		return getModelCoordinate(getViewportHeight());
	}

	/**
	 * @return viewport width in pixels.
	 */
	public static int getViewportWidth() {
		return Gdx.graphics.getWidth();
	}

	/***
	 * @return viewport height in pixels.
	 */
	public static int getViewportHeight() {
		return Gdx.graphics.getHeight();
	}
}
