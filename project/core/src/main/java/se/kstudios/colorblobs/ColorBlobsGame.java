package se.kstudios.colorblobs;

import com.badlogic.gdx.Game;

import se.kstudios.colorblobs.controllers.screens.GameplayScreen;

public class ColorBlobsGame extends Game {
	
	@Override
	public void create() {
		setScreen(new GameplayScreen());
	}

}
