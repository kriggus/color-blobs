package se.kstudios.colorblobs.utils;

import java.math.BigDecimal;

import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Vector2;

import se.kstudios.colorblobs.models.Blob;

public class MathUtil {
	
	public static boolean isInRange(float min, float max, float value, boolean includeBoundary) {
		if (includeBoundary)
			return isInRangeInclude(min, max, value);
		else
			return isInRangeExclude(min, max, value);
	}
	
	private static boolean isInRangeInclude(float min, float max, float value) {
		return value <= max && value >= min; 
	}
	
	private static boolean isInRangeExclude(float min, float max, float value) {
		return value < max && value > min;
	}
	
	public static boolean isInRange(int min, int max, int value, boolean includeBoundary) {
		if (includeBoundary)
			return isInRangeInclude(min, max, value);
		else
			return isInRangeExclude(min, max, value);
	}
	
	private static boolean isInRangeInclude(int min, int max, int value) {
		return value <= max && value >= min; 
	}
	
	private static boolean isInRangeExclude(int min, int max, int value) {
		return value < max && value > min;
	}
	
	public static float round(float value, int decimalPlaces) {
        BigDecimal bigDecimal = new BigDecimal(Float.toString(value));
        bigDecimal = bigDecimal.setScale(decimalPlaces, BigDecimal.ROUND_HALF_UP);
        return bigDecimal.floatValue();
    }

	public static float max(float[] values) {
		assert(values != null);
		assert(values.length > 0);
		
		float max = Float.NEGATIVE_INFINITY;
		for(float value : values) {
			max = Math.max(max, value);
		}
		return max;
	}

	public static float min(float[] values) {
		assert(values != null);
		assert(values.length > 0);
		
		float min = Float.POSITIVE_INFINITY;
		for (float value : values) {
			min = Math.min(min, value);
		}
		return min;
	}
	
	private static Vector2 intersection = new Vector2();
	public static boolean intersectSegments(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4) {
		return MathUtil.intersectSegments(x1, y1, x2, y2, x3, y3, x4, y4, intersection) > 0;
	}
	
	/**
	 * Check number of intersections between two line segments. Coordinate 1 and 2 defines 
	 * segment 1 while coordinate 3 and 4 defines segment 2.
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @param x3
	 * @param y3
	 * @param x4
	 * @param y4
	 * @param intersection Coordinate of the intersection if one occurred. For infinite intersections, closest to (x1, y1) is returned.
	 * @return number of intersections. Integer.MAX_VALUE is used to indicate infinite number of intersections.
	 */
	public static int intersectSegments(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4, Vector2 intersection) {
		assert(intersection != null);
		
		//get line1 in general form, ax + by + c = 0
		float a1 = y2 - y1;
		float b1 = x1 - x2;
		float c1 = a1*x1 + b1*y1;

		//get line2 in general form, ax + by + c = 0
		float a2 = y3 - y4;
		float b2 = x4 - x3;
		float c2 = a2*x3 + b2*y3;
		
		float det = a1*b2 - a2*b1;
		det = round(det, 4);
		
		if (det == 0f) { //parallel
			//normalize c for same line test
			if (a1 != 0) {
				c1 = c1 / a1;
			}
			else if (b1 != 0) {
				c1 = c1 / b1;
			}
			c1 = round(c1, 4);
			
			if (a2 != 0) {
				c2 = c2 / a2;
			}
			else if (b2 != 0) {
				c2 = c2 / b2;	
			}
			c2 = round(c2, 4);
			
			if (c1 == c2) { //same line!
				boolean is1Between3and4 = isBetween(x1, y1, x3, y3, x4, y4);
				boolean is2Between3And4 = isBetween(x2, y2, x3, y3, x4, y4);
				boolean is3Between1And2 = isBetween(x3, y3, x1, y1, x2, y2);
				boolean is4Between1And2 = isBetween(x4, y4, x1, y1, x2, y2);
				if (is1Between3and4 || is2Between3And4 || is3Between1And2 || is4Between1And2) { //is overlapping
					if (is1Between3and4) { 
						intersection.set(x1, y1);
					}
					else if (Vector2.dst2(x1, y1, x3, y3) < Vector2.dst2(x1, y1, x4, y4)) {
						intersection.set(x3, y3);
					}
					else {
						intersection.set(x4, y4);
					}
					//remark: point 2 is only closest to point 1 when point 2 is equal to 3 or 4, otherwise no overlapping.
					return Integer.MAX_VALUE;
				}
			}
			return 0;
		}
		else {
			float x = (b2*c1 - b1*c2) / det;
			float y = (a1*c2 - a2*c1) / det;
			x = round(x, 4);
		 	y = round(y, 4);
			
			if (!(isBetween(x, y, x1, y1, x2, y2) && isBetween(x, y, x3, y3, x4, y4))) {
				return 0;
			}
			else {
				intersection.set(x, y);
				return 1;
			}
		}
	}
	
	public static boolean isBetween(float x, float y, float x1, float y1, float x2, float y2) {
		return x <= Math.max(x1, x2)
			&& x >= Math.min(x1, x2)
			&& y <= Math.max(y1, y2)
			&& y >= Math.min(y1, y2);
	}

	public static float hypotenuse(float c1, float c2) {
		assert(c1 >= 0f);
		assert(c2 >= 0f);
		return (float)Math.sqrt(Math.pow(c1, 2) + Math.pow(c2, 2));
	}

	public static float area(float radius) {
		return (float) (Math.pow(radius, 2) * Math.PI);
	}

	public static float area(float mass, float density) {
		return Math.abs(mass / density);
	}

	public static float density(float mass, float radius) {
		float area = area(radius);
		return mass / area;
	}

	public static float mass(float radius, float dencity) {
		float area = area(radius);
		return dencity * area;
	}

	public static float meanRadius(float width, float height) {
		if (width == 0f) {
			return 0f;
		}
		if (height == 0f) {
			return 0f;
		}
		return (Math.abs(width) + Math.abs(height)) / 4;
	}

	public static float radius(float circleArea) {
		return (float) (Math.sqrt(Math.abs(circleArea) / Math.PI));
	}

	public static float radius(float mass, float density) {
		float area = area(mass, density);
		return radius(area);
	}

	public static boolean blobsOverlap(Blob b1, Blob b2) {
		Vector2 pos1 = b1.getPosition();
		Vector2 pos2 = b2.getPosition();
		return circlesOverlap(pos1.x, pos1.y, b1.getRadius(), pos2.x, pos2.y, b2.getRadius());
	}

    public static boolean circleOverlapBlob(float x, float y, float radius, Blob blob) {
        Vector2 blobPos = blob.getPosition();
        return circlesOverlap(x, y, radius, blobPos.x, blobPos.y, blob.getRadius());
    }

	private static Circle c1 = new Circle();
	private static Circle c2 = new Circle();
	public static boolean circlesOverlap(float x1, float y1, float radius1, float x2, float y2, float radius2) {
		c1.set(x1, y1, radius1);
		c2.set(x2, y2, radius2);
		return  Intersector.overlaps(c1, c2);
	}

}

//KEEP FOR UNTIL SURE TO THROW AWAY
//
//public static boolean onLine(float x, float y, float x1, float y1, float x2, float y2) {
//	float v1X = x2 - x1;
//	float v1Y = y2 - y1;
//	float v2X = x1 - x;
//	float v2Y = y1 - y;
//	float crs = v1X * v2Y - v1Y * v2X;
//	crs = MathUtil.round(crs, 4);
//	return crs == 0;
//}
//
//public static boolean sameLine(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4) {
//	get line1 in general form, ax + by + c = 0
//	float a1 = y2 - y1;
//	float b1 = x1 - x2;
//	float c1 = a1*x1 + b1*y1;
//
//	//get line2 in general form, ax + by + c = 0
//	float a2 = y3 - y4;
//	float b2 = x4 - x3;
//	float c2 = a2*x3 + b2*y3;
//	
//	float det = a1*b2 - a2*b1;
//	det = round(det, 4);
//	
//	//normalize c for same line test
//	if (a1 != 0) {
//		c1 = c1 / a1;
//	}
//	else if (b1 != 0) {
//		c1 = c1 / b1;
//	}
//	c1 = round(c1, 4);
//	
//	if (a2 != 0) {
//		c2 = c2 / a2;
//	}
//	else if (b2 != 0) {
//		c2 = c2 / b2;	
//	}
//	c2 = round(c2, 4);
//	
//	return det == 0 &&  c1 == c2;
//}