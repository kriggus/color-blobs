package se.kstudios.colorblobs.models;

import java.util.EnumSet;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;

import se.kstudios.colorblobs.utils.ColorUtil;
import se.kstudios.colorblobs.utils.ColorUtil.ColorComponent;
import se.kstudios.colorblobs.utils.MathUtil;

public class ColorComponentChanger implements ColorChangeable {

	private Color color;
	private float changeSpeed; //between 0 and 1.
	private int changeDirection; //-1 or 1
	private ColorComponent changingComponent;
	
	public ColorComponentChanger(Color color, float changeSpeed) {
		assert ColorUtil.isFull(color.r)  || ColorUtil.isFull(color.g)  || ColorUtil.isFull(color.b);
		assert ColorUtil.isEmpty(color.r) || ColorUtil.isEmpty(color.g) || ColorUtil.isEmpty(color.b);
		assert changeSpeed > 0f && changeSpeed < 1f;
		
		this.color = color.cpy();
		this.changeSpeed = changeSpeed;
		
		updateChangingComponent();
		updateChangeDirection();
	}

	@Override
	public Color getColor() {
		return color;
	}

	@Override
	public void update(float delta) {
		float value = getColorValueOfChangingComponent();
		if (value == 0f || value == 1f) {
			updateChangingComponent();
			updateChangeDirection();
			value = getColorValueOfChangingComponent();
		}
		
		float newValue = value + delta * changeSpeed * changeDirection;
		newValue = MathUtil.round(newValue, 5);
		newValue = MathUtils.clamp(newValue, 0f, 1f);
		setColorValueOfChaningComponent(newValue);
	}

	private float getColorValueOfChangingComponent() {
		switch (changingComponent) {
		case Red:
			return color.r;
		case Green:
			return color.g;
		case Blue:
			return color.b;
		default:	
			throw new UnsupportedOperationException("Color component value is not supported");
		}
	}
	
	private void setColorValueOfChaningComponent(float value) {
		switch (changingComponent) {
		case Red:
			color.r = value;
			break;
		case Green:
			color.g = value;
			break;
		case Blue:
			color.b = value;
			break;
		default:
			throw new UnsupportedOperationException("Color component value is not supported");
		}
	}
	
	private void updateChangeDirection() {
		float chaningValue = getColorValueOfChangingComponent();
		if (chaningValue == 0f) {
			changeDirection = 1;
		}
		else if (chaningValue == 1f) {
			changeDirection = -1;
		}
		else {
			throw new UnsupportedOperationException("Cannot update change during color change");
		}
	}
	
	private void updateChangingComponent() {
		assert ColorUtil.isFull(color.r)  || ColorUtil.isFull(color.g)  || ColorUtil.isFull(color.b);
		assert ColorUtil.isEmpty(color.r) || ColorUtil.isEmpty(color.g) || ColorUtil.isEmpty(color.b);
		assert (ColorUtil.isFull(color.r) || ColorUtil.isEmpty(color.r)) 
			&& (ColorUtil.isFull(color.g) || ColorUtil.isEmpty(color.g))
			&& (ColorUtil.isFull(color.b) || ColorUtil.isEmpty(color.b));
			
		EnumSet<ColorComponent> full = ColorUtil.getColorComponents(v -> ColorUtil.isFull(v), color);
        EnumSet<ColorComponent> empty = ColorUtil.getColorComponents(v -> ColorUtil.isEmpty(v), color);
		
        if (full.size() == 2) {
        	changingComponent = ColorUtil.getRandomColorComponent(empty.iterator().next());
		}
		else if (empty.size() == 2) {
			changingComponent = ColorUtil.getRandomColorComponent(full.iterator().next());
		}
	}
}
