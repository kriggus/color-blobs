package se.kstudios.colorblobs.controllers.input;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

import se.kstudios.colorblobs.controllers.GameplayCamera;
import se.kstudios.colorblobs.models.Blob;
import se.kstudios.colorblobs.utils.RenderUtil;

public class ClickInputProcessor extends GameplayInputProcessor {

	//remark: all time in seconds
	
	private float pushTime;
	private float pushTimeToApplyMax;

    //todo detect case when onscreen keyboard is closed by the android back button.
    private boolean isOnscreenKeyboardVisible;

	public ClickInputProcessor(Blob targetBlob, GameplayCamera camera, int screenWidth, int screenHeight) {
		super(targetBlob, camera, screenWidth, screenHeight);
		pushTimeToApplyMax = Blob.APPLY_FORCE_OVER_TIME_TIME * 2f;
        isOnscreenKeyboardVisible = false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		if (pointerCount() == 1) {
            if (isOnscreenKeyboardVisible) {
                Gdx.input.setOnscreenKeyboardVisible(false);
                isOnscreenKeyboardVisible = false;
            } else if ((screenHeight - screenY) < 60) {
                Gdx.input.setOnscreenKeyboardVisible(true);
                isOnscreenKeyboardVisible = true;
            } else {
                float targetPxX = RenderUtil.getPixelCoordinate(targetBlob.getPosition().x);
                float targetPxY = RenderUtil.getPixelCoordinate(targetBlob.getPosition().y);
                Vector3 targetPxPos = new Vector3(targetPxX, targetPxY, 0f);
                Vector3 targetScreenPos = camera.project(targetPxPos);

                float pointerX = pointerPosition(0).x;
                float pointerY = camera.viewportHeight - pointerPosition(0).y; //pointers seems to calculate (0,0) from viewport top left.

                Vector2 applyForce = new Vector2(targetScreenPos.x - pointerX, targetScreenPos.y - pointerY);
                applyForce.nor();
                applyForce.rotate(-camera.getRotationDegrees()); // compensate for that the model never rotates.

                if (pushTime >= pushTimeToApplyMax) {
                    float force = targetBlob.maxApplyForceOverTimeImproved();
                    applyForce.setLength(force);
                } else {
                    float pushTimeModifier = pushTime / pushTimeToApplyMax;
                    applyForce.setLength(targetBlob.maxApplyForceOverTime() * pushTimeModifier);
                }

                targetBlob.applyForceOverTime(applyForce);
                pushTime = 0f;
            }
		}
		return super.touchUp(screenX, screenY, pointer, button);
	}

    @Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void update(float delta) {
		if (pointerCount() == 1) {
			pushTime += delta;
		}
	}
}
