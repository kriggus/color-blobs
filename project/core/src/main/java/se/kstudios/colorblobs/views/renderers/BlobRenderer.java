package se.kstudios.colorblobs.views.renderers;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Disposable;

import se.kstudios.colorblobs.controllers.GameplayCamera;
import se.kstudios.colorblobs.models.Blob;
import se.kstudios.colorblobs.utils.RenderUtil;
import se.kstudios.colorblobs.views.RenderOptions;

public class BlobRenderer extends Renderer<Blob> implements Disposable {

	private static final Color BLOB_BORDER_COLOR = Color.BLACK;
	private static final float BLOB_BORDER_MAX_WIDTH = 30f;
	private static final float BLOB_BORDER_THICKNESS_TO_RADIUS_RATIO = 5f;
	private static final Color BLOB_DEBUG_BORDER_COLOR = Color.CYAN;
	private static final int BLOB_CIRCLE_SEGMENTS = 15;

	private ShapeRenderer shapeRenderer;
	
	public BlobRenderer() {
		shapeRenderer = new ShapeRenderer();
    }

	@Override
	public void update(GameplayCamera camera) {
		shapeRenderer.setProjectionMatrix(camera.combined);
	}

    public void begin() {
        if (RenderOptions.debugRender) {
            shapeRenderer.begin(ShapeType.Line);
        } else {
            shapeRenderer.begin(ShapeType.Filled);
        }
    }

    public void end() {
        shapeRenderer.end();
    }

	@Override
	protected void defaultRender(Blob model) {
		Vector2 modelPos = model.getPosition();
		float screenPosX = RenderUtil.getPixelCoordinate(modelPos.x);
		float screenPosY = RenderUtil.getPixelCoordinate(modelPos.y);
		float screenRadius = RenderUtil.getPixelCoordinate(model.getRadius());

        shapeRenderer.setColor(BLOB_BORDER_COLOR);
        shapeRenderer.circle(screenPosX, screenPosY, screenRadius, BLOB_CIRCLE_SEGMENTS);

		float borderThickness = Math.min(BLOB_BORDER_MAX_WIDTH, screenRadius / BLOB_BORDER_THICKNESS_TO_RADIUS_RATIO);

		shapeRenderer.setColor(model.getColor());
		shapeRenderer.circle(screenPosX, screenPosY, screenRadius - borderThickness, BLOB_CIRCLE_SEGMENTS);
	}

	@Override
	protected void debugRender(Blob model) {
		Vector2 modelPos = model.getPosition();
		float screenPosX = RenderUtil.getPixelCoordinate(modelPos.x);
		float screenPosY = RenderUtil.getPixelCoordinate(modelPos.y);
		float screenRadius = RenderUtil.getPixelCoordinate(model.getRadius());

		shapeRenderer.setColor(BLOB_DEBUG_BORDER_COLOR);
		shapeRenderer.circle(screenPosX, screenPosY, screenRadius, BLOB_CIRCLE_SEGMENTS);
		shapeRenderer.line(screenPosX, screenPosY + (screenRadius / 3f), screenPosX, screenPosY + screenRadius);
	}
	
	@Override
	public void dispose() {
		shapeRenderer.dispose();
	}
}
