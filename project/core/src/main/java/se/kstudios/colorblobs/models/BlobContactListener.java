package se.kstudios.colorblobs.models;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;

public class BlobContactListener implements ContactListener {

	private Map map;
	private Blob playerBlob;
	private Blob bigBlob;
	private Blob smallBlob;
	
	public BlobContactListener(Map map, Blob playerBlob) {
		this.map = map;
		this.playerBlob = playerBlob;
	}
	
	@Override
	public void beginContact(Contact contact) {
		Blob blobA = (Blob)contact.getFixtureA().getBody().getUserData();
		Blob blobB = (Blob)contact.getFixtureB().getBody().getUserData();
		updateBigAndSmal(blobA, blobB);
		if (smallBlob == playerBlob) {
			System.out.println("Player blob eaten!");
		}
		else {
			map.mergeBlobs(bigBlob, smallBlob);
		}
		bigBlob = null;
		smallBlob = null;
	}

	private void updateBigAndSmal(Blob b1, Blob b2) {
		if (b1.getMass() >= b2.getMass()) {
			bigBlob = b1;
			smallBlob = b2;
		} 
		else {
			bigBlob = b2;
			smallBlob = b1;
		}
	}
	
	@Override
	public void endContact(Contact contact) {
		//Do nothing
	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {
		//Do nothing
	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {
		//Do nothing
	}
}
