package se.kstudios.colorblobs.models;

public class MemberTextPresentation<T> {
	private String description;
	private MemberTextGetter<T> textGetter;
	
	public MemberTextPresentation(MemberTextGetter<T> textGetter, String description) {
		this.description = description;
		this.textGetter = textGetter;
	}

	public String getText(String format, T object) {
		return String.format(format, getDescriptionText(), getMemberText(object));
	}
	
	public String getMemberText(T object) {
		return textGetter.getText(object);
	}
	
	public String getDescriptionText() {
		return description;
	}
}
