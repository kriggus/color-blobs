package se.kstudios.colorbolobs.testutils;

import org.junit.AfterClass;
import org.junit.BeforeClass;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public abstract class LwjglApplicationDependentTest {

	private static LwjglApplication app;
	
	@BeforeClass
	public static void setUpBeforeClass() {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
	    cfg.useGL30 = true;
	    cfg.width = 480;
	    cfg.height = 320;
	    cfg.resizable = true;
	    app = new LwjglApplication(new TestApplicationListener(), cfg);
	}
	
	@AfterClass
	public static void tearDownAfterClass() {
		if (app != null) {
			app.exit();
			app = null;
		}
	}
}
