package se.kstudios.colorbolobs.testutils;

import org.junit.AfterClass;
import org.junit.BeforeClass;

import se.kstudios.colorblobs.models.Box2dWorldManager;

public abstract class Box2dWorldDependentTest extends LwjglApplicationDependentTest {

	@BeforeClass
	public static void setUpBeforeClass() {
		Box2dWorldManager.getInstance().createWorld();
	}

	@AfterClass
	public static void tearDownAfterClass() {
		Box2dWorldManager.getInstance().disposeWorld();
	}
}
