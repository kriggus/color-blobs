package se.kstudios.colorblobs.utils;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class MathUtil_tests {
	@Test
	public void area0radius() {
		float result = MathUtil.area(0f);
		assertEquals(0f, result, 0f);
	}
	
	@Test
	public void area_13radius() {
		float result = MathUtil.area(13f);
		assertEquals(530.929f, result, 0.001f);
	}
	
	@Test
	public void area_negradius() {
		float result = MathUtil.area(-10f);
		assertEquals(314.159f, result, 0.001f);
	}
	
	@Test
	public void area_0mass() {
		float result = MathUtil.area(0f, 10f);
		assertEquals(0f, result, 0f);
	}	

	@Test
	public void area_negMass() {
		float result = MathUtil.area(-0.5f, 5f);
		assertEquals(0.1f, result, 0.1f);
	}
	
	@Test
	public void area_0density() {
		float result = MathUtil.area(1.1f, 0f);
		assertEquals(Float.POSITIVE_INFINITY, result, 0f);
	}
	
	@Test
	public void area_negDensity() {
		float result = MathUtil.area(1.1f, -0.2f);
		assertEquals(5.5f, result, 0.1f);
	}
	
	@Test
	public void area_0dot3density_1001mass() {
		float result = MathUtil.area(0.3f, 1001f);
		assertEquals(0.000299f, result, 0.000001f);
	}
	
	@Test
	public void density_0mass() {
		float result = MathUtil.density(0f, 10f);
		assertEquals(0f, result, 0f);
	}
	
	@Test
	public void density_negMass() {
		float result = MathUtil.density(-10.1f, 12.112f);
		assertEquals(-0.021f, result, 0.001f);
	}
	
	@Test
	public void density_0radius() {
		float result = MathUtil.density(13f, 0f);
		assertEquals(Float.POSITIVE_INFINITY, result, 0f);
	}
	
	@Test
	public void density_negRadius() {
		float result = MathUtil.density(19090f, -12);
		assertEquals(42.198, result, 0.001f);
	}
	
	@Test
	public void density_1231dot03mass_19dot99radius() {
		float result = MathUtil.density(1231.03f, 19.99f);
		assertEquals(0.980f, result, 0.001f);
	}

	@Test
	public void mass_0radius() {
		float result = MathUtil.mass(0, 0.19f);
		assertEquals(0f, result, 0f);
	}

	@Test
	public void computeMass_negRadius() {
		float result = MathUtil.mass(-12, 0.23f);
		assertEquals(104.049f, result, 0.001f);
	}
	
	@Test
	public void computeMass_0dencity() {
		float result = MathUtil.mass(12f, 0f);
		assertEquals(0f, result, 0f);
	}
	
	@Test
	public void computeMass_negDencity() {
		float result = MathUtil.mass(23f, -12);
		assertEquals(-19942.830f, result, 0.001f);
	}

	@Test
	public void computeMass_0dot001radius_999991dot9dencity() {
		float result = MathUtil.mass(0.001f, 999991.9f);
		assertEquals(3.141f, result, 0.001f);
	}

	@Test
	public void meanRadius_0width() {
		float result = MathUtil.meanRadius(0f, 23f);
		assertEquals(0f, result, 0f);
	}

	@Test
	public void meanRadius_negWidth() {
		float result = MathUtil.meanRadius(-12, 32.4f);
		assertEquals(11.1f, result, 0.1f);
	}
	
	@Test
	public void meanRadius_0height() {
		float result = MathUtil.meanRadius(12.3f, 0);
		assertEquals(0f, result, 0f);
	}

	@Test
	public void meanRadius_negHeight() {
		float result = MathUtil.meanRadius(10, -12);
		assertEquals(5.5f, result, 0.1f);
	}
	
	@Test
	public void meanRadius_17width_10dot3height() {
		float result = MathUtil.meanRadius(17f, 10.3f);
		assertEquals(6.825, result, 0.001f);
	}

	@Test
	public void radius_0area() {
		float result = MathUtil.radius(0f);
		assertEquals(0f, result, 0f);
	}

	@Test
	public void radius_negArea() {
		float result = MathUtil.radius(-12);
		assertEquals(1.954f, result, 0.001f);
	}
	
	@Test
	public void radius_1881dot817area() {
		float result = MathUtil.radius(1881.617f);
		assertEquals(24.473, result, 0.001f);
	}

	@Test
	public void radius_0mass() {
		float result = MathUtil.radius(0f, 1.1f);
		assertEquals(0f, result, 0f);
	}
	
	@Test
	public void radius_negMass() {
		float result = MathUtil.radius(-11, 1.01f);
		assertEquals(1.861, result, 0.001f);
	}
	
	@Test
	public void radius_0density() {
		float result = MathUtil.radius(1212f, 0f);
		assertEquals(Float.POSITIVE_INFINITY, result, 0.001f);
	}
	
	@Test
	public void radius_negDensity() {
		float result = MathUtil.radius(112f, -12f);
		assertEquals(1.723f, result, 0.001f);
	}
	
	@Test
	public void radius_91dot1mass_1dot001dencity() {
		float result = MathUtil.radius(91.1f, 1.001f);
		assertEquals(5.382f, result, 0.001f);
	}
}
