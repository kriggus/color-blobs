package se.kstudios.colorblobs.models;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Test;

import com.badlogic.gdx.physics.box2d.World;

public class Box2dWorldManager_tests {

	@After
	public void tearDown() {
		Box2dWorldManager.getInstance().dispose();
	}
	
	@Test
	public void createWorldTest() {
		Box2dWorldManager.getInstance().createWorld();
		
		World world = Box2dWorldManager.getInstance().getWorld();
		assertNotNull(world);
	}
	
	@Test 
	public void disposeWorldTest_isDisposed() {
		Box2dWorldManager.getInstance().disposeWorld();
		World world = Box2dWorldManager.getInstance().getWorld();
		assertTrue(world == null);
	}
	
	@Test
	public void disposeWorldTest_isNotDisposed() {
		Box2dWorldManager.getInstance().createWorld();
		Box2dWorldManager.getInstance().disposeWorld();

		World world = Box2dWorldManager.getInstance().getWorld();
		assertTrue(world == null);
	}
	
	@Test
	public void getNextObjectNumberTest_noWorld() {
		int result = Box2dWorldManager.getInstance().getNextObjectNumber();
		assertEquals(-1, result);
	}

	@Test
	public void getNextObjectNumberTest_hasWorld() {
		Box2dWorldManager.getInstance().createWorld();
		int result;
		
		result = Box2dWorldManager.getInstance().getNextObjectNumber();
		assertEquals(0, result);
		
		result = Box2dWorldManager.getInstance().getNextObjectNumber();
		assertEquals(1, result);
		
		result = Box2dWorldManager.getInstance().getNextObjectNumber();
		assertEquals(2, result);
		
		result = Box2dWorldManager.getInstance().getNextObjectNumber();
		assertEquals(3, result);
	}
	
	@Test
	public void getNextObjectNameTest() {
		Box2dWorldManager.getInstance().createWorld();
		String result;
		
		result = Box2dWorldManager.getInstance().getNextObjectName(Blob.class);
		assertEquals("Blob_0", result);
		
		result = Box2dWorldManager.getInstance().getNextObjectName(Blob.class);
		assertEquals("Blob_1", result);
	}
}
