package se.kstudios.colorblobs.utils;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.badlogic.gdx.math.Vector2;

public class RenderUtil_tests {

	@Test
	public void getScreenPosition() {
		Vector2 modelPos = new Vector2(1.12f, 3443.12f);
		Vector2 scrPos = RenderUtil.getPixelPosition(modelPos);
		assertEquals(modelPos.x * RenderUtil.MODEL_TO_PIXEL_RATIO, scrPos.x, 0.0001f);
		assertEquals(modelPos.y * RenderUtil.MODEL_TO_PIXEL_RATIO, scrPos.y, 0.0001f);
	}
	
	@Test
	public void getModelPosition() {
		Vector2 scrPos = new Vector2(31.98f, 43.12f);
		Vector2 modelPos = RenderUtil.getModelPosition(scrPos);
		assertEquals(scrPos.x / RenderUtil.MODEL_TO_PIXEL_RATIO, modelPos.x, 0.0001f);
		assertEquals(scrPos.y / RenderUtil.MODEL_TO_PIXEL_RATIO, modelPos.y, 0.0001f);
	}
}
