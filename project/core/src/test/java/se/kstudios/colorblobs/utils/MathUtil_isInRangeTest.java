package se.kstudios.colorblobs.utils;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class MathUtil_isInRangeTest {
	
	@Parameter(value = 0)
	public float min;
	
	@Parameter(value = 1)
	public float max;
	
	@Parameter(value = 2)
	public float value;
	
	@Parameter(value = 3)
	public boolean includeBoundary;
	
	@Parameter(value = 4)
	public boolean expected;
	
	@Parameters
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] {
			{-1.1f, -0.5f, -0.5f, true, true},
			{-1.1f, -0.5f, -0.6f, false, true},
			{-1.1f, -0.5f, -0.5f, false, false},
			{-1.1f, -0.5f, -0.4f, true, false},
			{12, 22, 12, true, true},
			{12, 22, 21, false, true},
			{12, 22, 22, false, false},
			{12, 22, 11, true, false}
		});
	}
	
	@Test
	public void runTests() {
		assertEquals(expected, MathUtil.isInRange(min	, max, value, includeBoundary));
	}
}
