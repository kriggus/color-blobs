package se.kstudios.colorblobs.utils;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import com.badlogic.gdx.math.Vector2;

@RunWith(Parameterized.class)
public class MathUtil_intersectSegmentsTest {
	
	@Parameter(value = 0)
	public float x1; 

	@Parameter(value = 1)
	public float y1;

	@Parameter(value = 2)
	public float x2;

	@Parameter(value = 3)
	public float y2;
	
	@Parameter(value = 4)
	public float x3;
	
	@Parameter(value = 5)
	public float y3;

	@Parameter(value = 6)
	public float x4;

	@Parameter(value = 7)
	public float y4;

	@Parameter(value = 8)
	public Vector2 intersection;
	
	@Parameter(value = 9)
	public int expected;
	
	@Parameter(value = 10)
	public Vector2 expectedIntersection;
	
	@Parameters
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] {
			{1f, -3f, 1f, 1f, 1f, 0f, 1f, 3f, new Vector2(), Integer.MAX_VALUE, new Vector2(1f, 0f)},
			{1f, 1f, 3f, 3f, 2f, 2f, 5f, 5f, new Vector2(), Integer.MAX_VALUE, new Vector2(2f, 2f)},
			{2f, 2f, 5f, 5f, 1f, 1f, 3f, 3f, new Vector2(), Integer.MAX_VALUE, new Vector2(2f, 2f)},
			{1f, 1f, 2f, 2f, 2f, 2f, 5f, 5f, new Vector2(), Integer.MAX_VALUE, new Vector2(2f, 2f)},
			{2f, 2f, 5f, 5f, 1f, 1f, 2.1f, 2.1f, new Vector2(), Integer.MAX_VALUE, new Vector2(2f, 2f)},
			{3f, -4f, 3f, 1f, 3f, -3f, 3f, -1f, new Vector2(), Integer.MAX_VALUE, new Vector2(3f, -3f)},
			{1f, 1f, 2f, 2f, 2.1f, 2.1f, 6f, 6f, new Vector2(), 0, new Vector2()},
			{1f, -3f, 1f, 1f, 0f, 0f, 1f, 1f, new Vector2(), 1, new Vector2(1f, 1f)},
			{1f, -3f, 1f, 3f, 0f, 0f, 1f, 1f, new Vector2(), 1, new Vector2(1f, 1f)},
			{1f, -3f, 1f, 1f, 0f, 0f, 2f, 2f, new Vector2(), 1, new Vector2(1f, 1f)},
			{1f, -3f, 1f, 1f, 0f, 0f, 1.99f, 2f, new Vector2(), 0, new Vector2()},
			{0f, 0f, 0f, 1f, 0f, 1f, 1f, 1f, new Vector2(), 1, new Vector2(0f, 1f)},
			{0f, 1f, 1f, 1f, 1f, 1f, 1f, 0f, new Vector2(), 1, new Vector2(1f, 1f)},
			{1f, 1f, 1f, 0f, 1f, 0f, 0f, 0f, new Vector2(), 1, new Vector2(1f, 0f)},
			{0f, 0f, 0f, 1f, 0f, 0f, 1f, 0f, new Vector2(), 1, new Vector2(0f, 0f)},
			{0f, 0f, 0f, 1f, 1f, 0f, 0f, 0f, new Vector2(), 1, new Vector2(0f, 0f)},
			{0f, 1f, 0f, 0f, 0f, 0f, 1f, 0f, new Vector2(), 1, new Vector2(0f, 0f)},
			{0f, 1f, 0f, 0f, 1f, 0f, 0f, 0f, new Vector2(), 1, new Vector2(0f, 0f)},
		});
	}
	
	@Test
	public void runTests() {	
		int result = MathUtil.intersectSegments(x1, y1, x2, y2, x3, y3, x4, y4, intersection);
		assertEquals(expected, result);
		if (result > 0)  {
			assertEquals(expectedIntersection, intersection);
		}
	}
}