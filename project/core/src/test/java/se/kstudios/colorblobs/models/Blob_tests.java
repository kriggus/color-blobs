package se.kstudios.colorblobs.models;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;

import se.kstudios.colorbolobs.testutils.Box2dWorldDependentTest;

public class Blob_tests extends Box2dWorldDependentTest {

	@Test
	public void updateVelocity_0Delta() {
		Blob blob = new Blob(10f, 0.43f, 0.2f, 0.2f, new Color(1f, 0.5f, 0.3f, 1f));
		blob.setPosition(0f, 0f);
		blob.setAcceleration(12f, 1f);

		assertEquals(0f, blob.getVelocity().x, 0.0001f);
		assertEquals(0f, blob.getVelocity().y, 0.0001f);
		
		blob.updateVelocity(0f);

		assertEquals(0f, blob.getVelocity().x, 0.0001f);
		assertEquals(0f, blob.getVelocity().y, 0.0001f);
	}

	@Test
	public void updateVelocity_0dot0012Delta() {
		Blob blob = new Blob(10f, 0.43f, 0.2f, 0.2f, new Color(1f, 0.5f, 0.3f, 1f));
		blob.setPosition(0f, 0f);
		blob.setAcceleration(12f, 1f);
		
		assertEquals(new Vector2(0, 0), blob.getVelocity());
		
		blob.updateVelocity(0.0012f);
		
		assertEquals(0.0144f, blob.getVelocity().x, 0.0001f);
		assertEquals(0.0012f, blob.getVelocity().y, 0.0001f);
	}
	
	@Test
	public void updateVelocity_0dot2116Delta() {
		Blob blob = new Blob(10f, 0.43f, 0.2f, 0.2f, new Color(1f, 0.5f, 0.3f, 1f));
		blob.setPosition(0f, 0f);
		blob.setAcceleration(12f, 0f);
		blob.setVelocity(0.3f, 3.3f);
		
		blob.updateVelocity(0.2116f);

		assertEquals(2.8392f, blob.getVelocity().x, 0.0001f);	
		assertEquals(3.3f, blob.getVelocity().y, 0.0001f);	
	}
}
