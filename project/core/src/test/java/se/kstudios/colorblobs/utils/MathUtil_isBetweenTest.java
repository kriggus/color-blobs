package se.kstudios.colorblobs.utils;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class MathUtil_isBetweenTest {
		
	@Parameter(value = 0)
	public float x;
	
	@Parameter(value = 1)
	public float y;
	
	@Parameter(value = 2)
	public float x1;
	
	@Parameter(value = 3)
	public float y1;
	
	@Parameter(value = 4)
	public float x2;
	
	@Parameter(value = 5)
	public float y2;
	
	@Parameter(value = 6)
	public boolean expected;
	
	@Parameters
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] {
			{1f, 1f, 1f, 1f, 2f, 1f, true},
			{1.2f, 1f, 1f, 1f, 2f, 1f, true},
			{1f, 1.2f, 1f, 1f, 2f, 1f, false},
		});
	}
	
	@Test
	public void runTests() {
		assertEquals(MathUtil.isBetween(x, y, x1, y1, x2, y2), expected);
	}
}
