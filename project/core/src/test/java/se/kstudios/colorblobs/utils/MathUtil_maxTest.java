package se.kstudios.colorblobs.utils;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class MathUtil_maxTest {
		
	@Parameter(value = 0)
	public float[] values;
	
	@Parameter(value = 1)
	public float expected;
	
	@Parameters
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] {
			{new float[] {-12f, -1f}, -1f},
			{new float[] {12, 12, 4, 1}, 12f},
			{new float[] {12, 12, 4, 12.1f, 1} , 12.1f}
		});
	}
	
	@Test
	public void runTests() {
		assertEquals(expected, MathUtil.max(values), 0.01f);
	}
}
