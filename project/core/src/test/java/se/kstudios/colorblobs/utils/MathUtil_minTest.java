package se.kstudios.colorblobs.utils;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class MathUtil_minTest {
		
	@Parameter(value = 0)
	public float[] values;
	
	@Parameter(value = 1)
	public float expected;
	
	@Parameters
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] {
			{new float[] {-12f, -1f}, -12f},
			{new float[] {12, 12, 4, 1}, 1f},
			{new float[] {12, 12, 4, 12.1f, 3.99f}, 3.99f}
		});
	}
	
	@Test
	public void runTests() {
		assertEquals(expected, MathUtil.min(values), 0.01f);
	}
}
