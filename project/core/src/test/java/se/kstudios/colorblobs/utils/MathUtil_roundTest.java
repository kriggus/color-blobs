package se.kstudios.colorblobs.utils;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

public class MathUtil_roundTest {
	
	@Parameter(value = 0)
	public float value;
	
	@Parameter(value = 1)
	public int decimalPlaces;
	
	@Parameter(value = 2)
	public float expected;
	
	@Parameter(value = 3)
	public float allowedDiff;
	
	@Parameters
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] {
			{3.33f, 0, 3f, 0.01f},
			{986.5001f, 4, 986.5001f, 0.000049f},
			{986.5005f, 3, 986.501f, 0.00049f},
			{-12.45f, 1, -12.5f,  0.0049f}
		});
	}
	
	@Test
	public void runTests() {
		float result = MathUtil.round(value, decimalPlaces);
		assertEquals(expected, result, allowedDiff);
	}
}
