package se.kstudios.colorblobs.utils;

import static org.junit.Assert.*;

import org.junit.Test;

import se.kstudios.colorblobs.utils.ColorUtil.ColorComponent;

public class ColorUtil_tests {

	@Test
	public void isEmpty_empty() {
		boolean result = ColorUtil.isEmpty(0.00f);
		assertTrue(result);
	}
	
	@Test
	public void isEmpty_nonEmpty() {
		boolean result = ColorUtil.isEmpty(0.001f);
		assertFalse(result);
	}
	
	@Test
	public void isFull_full() {
		boolean result = ColorUtil.isFull(1.000f);
		assertTrue(result);
	}
	
	@Test
	public void isFull_nonEmpty() {
		boolean result = ColorUtil.isFull(0.0f);
		assertFalse(result);
	}
	
	@Test
	public void getRandomColorComponent_notFobidden() {
		ColorComponent forbidden = ColorComponent.Blue;
		for (int i = 0; i < 100; i++) {
			ColorComponent result = ColorUtil.getRandomColorComponent(forbidden);
			assertNotEquals(forbidden, result);
		}
	}
}
