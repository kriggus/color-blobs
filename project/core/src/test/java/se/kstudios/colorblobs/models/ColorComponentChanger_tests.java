package se.kstudios.colorblobs.models;

import static org.junit.Assert.*;

import org.junit.Test;

import com.badlogic.gdx.graphics.Color;

public class ColorComponentChanger_tests {
	
	@Test
	public void update_colorChangesCorrectly() {
		ColorComponentChanger changer = new ColorComponentChanger(Color.RED, 0.1f);

		boolean isRed = changer.getColor().b == 0f 
				|| changer.getColor().g == 0f 
				|| changer.getColor().r == 1f;

		assertTrue(isRed);
		
		for(int i = 0; i < 99; i++) {
			changer.update(0.1f);
			assertEquals(1f, changer.getColor().r, 0.0001f);
			
			boolean hasNonEmptyBlueOrGreen = changer.getColor().b != 0f 
					|| changer.getColor().g != 0f;
			
			assertTrue(hasNonEmptyBlueOrGreen);
		}

		changer.update(0.1f);
		
		boolean hasFullBlueOrGreen = changer.getColor().b == 1f 
				|| changer.getColor().g == 1f;
		
		assertTrue(hasFullBlueOrGreen);
		
		changer.update(0.1f);
		
		boolean hasEmptyChannel = changer.getColor().r == 0f 
				|| changer.getColor().g == 0f 
				|| changer.getColor().b == 0f;
		
		boolean hasHalfFullChannel =
					   (changer.getColor().r > 0f && changer.getColor().r < 1f)
				  	|| (changer.getColor().g > 0f && changer.getColor().g < 1f)
				  	|| (changer.getColor().b > 0f && changer.getColor().b < 1f);
		
		boolean hasFullChannel = changer.getColor().r == 1f 
				|| changer.getColor().g == 1f 
				|| changer.getColor().b == 1f;

		assertTrue(hasEmptyChannel);
		assertTrue(hasHalfFullChannel);
		assertTrue(hasFullChannel);
	}
}
