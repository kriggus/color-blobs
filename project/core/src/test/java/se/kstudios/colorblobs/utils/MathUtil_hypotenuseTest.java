package se.kstudios.colorblobs.utils;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class MathUtil_hypotenuseTest {
		
	@Parameter(value = 0)
	public float c1;
	
	@Parameter(value = 1)
	public float c2;
	
	@Parameter(value = 2)
	public float expected;
	
	@Parameters
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] {
			{0f, 0f, 0f},
			{1f, 1f, 1.414f},
			{1f, 2f, 2.236f},
			{2f, 1f, 2.236f},
			{4f, 7f, 8.062f},
		});
	}
	
	@Test
	public void runTests() {
		assertEquals(MathUtil.hypotenuse(c1, c2), expected, 0.001f);
	}
}
